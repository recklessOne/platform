var _vsp = argument[0];
if sign(_vsp) != 0{
	var _blockOnMeList = instance_place_wall_list(x,y-1);
	for(var b = 0; b < ds_list_size(_blockOnMeList); b++){
		var _blockOnMe = ds_list_find_value(_blockOnMeList,b);
		if _blockOnMe != noone and instance_exists(_blockOnMe)
		and _blockOnMe.pushable and object_is_ancestor(_blockOnMe.object_index,oEntity){
			if _blockOnMe.bbox_bottom <= bbox_top{
				with _blockOnMe{
					scr_blockOnMe(_vsp);
				}
			}
			_blockOnMe.y += _vsp;
		}
	}
}