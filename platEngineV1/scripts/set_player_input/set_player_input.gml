//Initialize keys - set to false by default for objects that will never use them

key_left     = keyboard_check_direct(vk_left)   || gamepad_axis_value(0,gp_axislh) < 0;
key_right    = keyboard_check_direct(vk_right)  || gamepad_axis_value(0,gp_axislh) > 0;
key_up       = keyboard_check_direct(vk_up)		|| gamepad_axis_value(0,gp_axislv) < 0;
key_down     = keyboard_check(vk_down)			|| gamepad_axis_value(0,gp_axislv) > 0;
key_jump     = keyboard_check_pressed(ord("X")) || gamepad_button_check_pressed(0,gp_face1); 
key_jumpHeld = keyboard_check(ord("X"))         || gamepad_button_check(0,gp_face1);
key_dash     = keyboard_check_pressed(ord("C")) || gamepad_button_check_pressed(0,gp_face3);
key_dashHeld = keyboard_check(ord("C"))         || gamepad_button_check(0,gp_face3); // only used if you use the dash_stop_when_release option
key_interact = keyboard_check_pressed(ord("A"))
