///@param hsp,vsp
var _hsp = argument[0];
var _vsp = argument[1];
if _hsp == 0 and _vsp == 0{ exit;}
var _id = id;
var _block = instance_place(x+_hsp,y+_vsp,oEntity);
if _block != noone and _block.pushable {///and _block != _id{// and _block.onground{
	with(_block){
		var _nextBlockList = instance_place_wall_list(x+_hsp,y+_vsp);
		var _nextBlockListSize = ds_list_size(_nextBlockList);
		if _nextBlockListSize == 0{//(not place_meeting_wall(x+_hsp,y)){
			if object_is_ancestor(object_index,oEntity){
				scr_blockOnMe(_vsp);
				x += _hsp;
				y += _vsp;
				//if object_index != oPlayer {
					/*if y > _id.y{
						_id.vsp = vsp;
					} else{
						vsp = _id.vsp;
					}*/
					if _id.y > y{
						vsp = _id.vsp; 
						if _id.hsp != 0{ hsp = _id.hsp;}  if heavy > 0 { hsp *= .5; }
					} else if !_id.heavy and y > _id.y{
						_id.vsp  = vsp; 
						if hsp != 0{ _id.hsp = hsp; } if _id.heavy > 0 { _id.hsp *= .5; }
					}
				
				//} else 
				if object_index == oPlayer and sign(_hsp) == -1 and sprite_index == sprite_climb { x -= _hsp; }					
			}
			if object_is_ancestor(_id.object_index,oEntity) and _hsp == 0{
			//and (_id.object_index != oPlayer or _hsp == 0){
				//with _id{scr_blockOnMe(_vsp);}
				//if _id.heavy == 0{
					//_id.x += _hsp;
					_id.y += _vsp;
			//	}
			//	
			}
		} else if _nextBlockListSize > 0 {
			//if there is more than 1 block, check and update them all, have them pass it recurrisivly
			var _end = false
			for(var _b = 0; _b < _nextBlockListSize and _end == false; _b++){
				var _nextBlock = ds_list_find_value(_nextBlockList,_b);
				var _push = noone;
				
				if (_nextBlock == noone or _nextBlock.pushable == true)
				and _nextBlock != id and _nextBlock != _id{
					with _nextBlock{
						_push = push_block(_hsp,_vsp);
					}
					if _push == noone or _push == -1{
						_push = push_block(_hsp,_vsp);
					} 
				}
				if !object_is_ancestor(_nextBlock.object_index,oEntity)
				or  _nextBlock.pushable == false
				or _push == -1 
				or _push == noone{
					_block = -2;
				}
			}
		}
	}
}

return _block;