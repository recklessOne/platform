///@param hsp
///@param slope_max

var _hsp       = argument0;
var _slope_max = argument1;
 if !place_meeting_wallone(){
//Move up over a slope
var dy = 0;
//show_debug_message("Slope UP while loop start");
while(place_meeting_wall(x+_hsp,y-dy))dy++;
if(dy <= _slope_max) y -= dy;
//show_debug_message("Slope UP while loop END");


//Move down over a slope
dy = 0;
//show_debug_message("Slope DOWN while loop start");
while(not place_meeting_wall(x+_hsp,y+dy+1))dy++;
if(dy <= _slope_max) y += dy;
//show_debug_message("Slope DOWN while loop END");
 }