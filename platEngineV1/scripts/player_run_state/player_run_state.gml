

if(is_begin_state()){
	change_sprite(sprite_run);
}

var inputX = key_right - key_left;//1 to right and -1 to left

if(onIce == true)hsp_acc_final  = hsp_acc_ice;
else			 hsp_acc_final  = hsp_acc;



if(inputX != 0){
	setFacing(inputX);
	//and we change a bit if acceleration is linear or not
	hsp += lin_acc? hsp_acc_final*inputX: (abs(hsp)+1)*hsp_acc_final*inputX; // increase hsp by rate of acceleration in the current direction of movement
	
	//stops over a wall
	//if(!place_meeting(x+sign(hsp),y,oPushBlock) && place_meeting_wall(x+sign(hsp),y-2) && !place_meeting(x+sign(hsp),y,oSlope))
		//hsp = 0;
		
	hsp = clamp(hsp,-hsp_walk,hsp_walk); // ensures speed cant go over our max walking speed, regardless of direction
}
else {
	change_state(player_idle_state);
}


if(not onground){change_state(player_air_state);}

entity_motion(true);
//Actions
player_jump();
player_dash();

