///@param x
///@param y
//check a wall, push block or an entity at pos (x,y)
var xx = argument0;
var yy = argument1;

var _wallList = ds_list_create();
var _wallNum = instance_place_list(xx,yy, par_collidable,_wallList,false);

for(var _w = _wallNum -1; _w >= 0; _w--){
	var _wallCheck = ds_list_find_value(_wallList,_w);
	if _wallCheck == noone or !instance_exists(_wallCheck) or _wallCheck == id{
		ds_list_delete(_wallList,_w);
	}
}

return _wallList;