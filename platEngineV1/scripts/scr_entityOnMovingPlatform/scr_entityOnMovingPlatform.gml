///@param x,y,obj,dist,dir
var _x = argument[0];
var _y = argument[1];
var _obj = argument[2];
//var _dist = argument[3];
//var _dir = argument[4];
if place_meeting(_x,_y, _obj){
	var _entityList = ds_list_create();
	var _numEntitys = instance_place_list(_x,_y,_obj,_entityList,false);
	for(var _e = 0; _e < _numEntitys; _e++){
		var _checkEntity = ds_list_find_value(_entityList,_e);
		if instance_exists(_checkEntity) and _checkEntity != noone
		and ds_list_find_index(entityToMoveList,_checkEntity) == -1 {
			ds_list_add(entityToMoveList,_checkEntity);
		}
	}
	ds_list_destroy(_entityList);
}