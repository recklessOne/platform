///@param list
///@param _color
///@param xScale
///@param yScale
///@param x0
///@param y0

var _list   = argument0;
var _color  = argument1;
var _xScale = argument2;
var _yScale = argument3;
var _x0     = argument4;
var _y0     = argument5;

draw_set_color(_color);

for(var i = 0; i < ds_list_size(_list);i++){
	var x2 = _x0+i*_xScale;
	var y2 = _y0 - _yScale*_list[|i];
	
	if(i == 0){
		var x1 = _x0;
		var y1 = _y0;		
	}
	else{
		var x1 = _x0 + (i-1)*_xScale;
		var y1 = _y0 - _yScale*_list[|i-1];		
	}
	
	//draw_line(x1,y1,x2,y2);
	//draw_point(x2,y2);
	draw_circle(x2,y2,2,false);
}