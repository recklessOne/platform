///@param canPushBlock

var _canPushBlock    = false;

if(argument_count == 1) _canPushBlock = argument[0];

hsp_frac       += hsp;
vsp_frac       += vsp;
var new_hsp     = round(hsp_frac);
if(vsp_frac > 0){ var new_vsp = floor(vsp_frac);}
else		    { var new_vsp = ceil(vsp_frac);}

hsp_frac       -= new_hsp;
vsp_frac       -= new_vsp;

if(abs(vsp)<1) {new_vsp = sign(vsp);}
if(step_over_asset()) == true{ exit;}
repeat(abs(new_vsp)){
	//for springs and others elements type
	if(step_over_asset()){
		break;	
	}	
    else if (not place_meeting_wall(x,y+sign(vsp)) and not place_meeting_wallone()){
		var _pb = push_block(0,sign(vsp));
		if _pb == noone or _pb == -1{
			y += sign(vsp);
		} else {
			var _ent = instance_place(x,y+vsp,oEntity);
			if _ent != noone and _ent.vsp != 0 and _ent.y > y and !heavy{
				vsp = _ent.vsp;
			}	
		}

	}else{// can only push on ground
		
		if vsp > 0{onground = true;}
		var _ent = instance_place(x,y+vsp,oEntity);
		if _ent != noone and _ent.y > y and !heavy{
			vsp = _ent.vsp;
		}/* else if heavy{
			vsp      = 0; // then stop movement
			vsp_frac = 0;	
			
		}*/
		break;	
	}
}


//Horizontal Collision
var _slope_max  = 2; // max height of a slope in pixels

for(var k = 0; k < abs(new_hsp);k++){
	// so slope code isnt triggered while in the air. 
	
	if(onground==true){ move_over_slope(sign(hsp),_slope_max); }//Move over a slope
	
    if (not place_meeting_wall(x+sign(hsp),y)){ x += sign(hsp);}
	else if(not _canPushBlock) and onground {// can only push on ground
		hsp      = 0; // then stop movement
		hsp_frac = 0;		
		break;		
	}else {		//push a block
		var _push = push_block(hsp,0);
		if(_push != -1 and _push != noone){
			new_hsp = max(1,new_hsp);//hsp_push_block;////// lowering the speed a bit
		} 
		if object_index == oPushBlock and abs(hsp) > 0{
			var _fric = sign(hsp) * - 1 * (.1 + (.1 * heavy));
			if abs(_fric) < abs(hsp){
				hsp += _fric;
			} else {
				hsp = 0;
			}
		}
	}
}
if onground{
	if abs(hsp) > 0{
		hsp -= sign(hsp) * (.05 + (.05*heavy));
	}
}