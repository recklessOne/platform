/// @param _xMove,_yMove
var _xMove = argument0;
var _yMove = argument1;
var _storeX = x;
var _storeY = y;
var _entity = oEntity;
var _switchDir = false;
// move the things on top of me
var _xMod = 3;//_traveledThisFrame *2;
scr_entityOnMovingPlatform(x, y - 2, _entity);
scr_entityOnMovingPlatform(x - _xMod, y,_entity);//,_traveledThisFrame,_dir);
scr_entityOnMovingPlatform(x + _xMod, y,_entity);//,_traveledThisFrame,_dir);
// for moving away from player
scr_entityOnMovingPlatform(_storeX, _storeY - 2, _entity);
scr_entityOnMovingPlatform(_storeX -_xMod, _storeY,_entity);//,_traveledThisFrame,_dir);
scr_entityOnMovingPlatform(_storeX +_xMod, _storeY,_entity);//,_traveledThisFrame,_dir);
var _numEntitys = ds_list_size(entityToMoveList);
for(var _e = 0; _e < _numEntitys; _e++){	
	var _checkEntity = ds_list_find_value(entityToMoveList,_e);
	if instance_exists(_checkEntity) and _checkEntity != noone{
		var _platform = id;
		with _checkEntity{
			platformon = _platform;
			var _onPlatform = instance_place(x,bbox_bottom-1,_platform);
			
			if !place_meeting_collision(x + _xMove,y) and (onground == false or _onPlatform != noone){
					var _hPush = push_block(_xMove,0);
					if _hPush != -2{
						x += _xMove;
					} else if _xMove != 0{
						_switchDir = true;
					}
				}
			// if not the ground or the ground is the block, then move w block
			if !place_meeting_collision(x,y+_yMove){
				if !place_meeting_wallone() or sign(_yMove) == -1{
					var _vPush = push_block(0,_yMove);
					if _vPush != -2{
						scr_blockOnMe(_yMove);
						y += _yMove;		
						if _yMove < 0 and object_index == oPlayer and !place_meeting_collision(x,y+1) and sprite_index != sprite_climb{ y +=1;}
					} else if _yMove != 0{
						_switchDir = true;
					}
				} 
			} 
				
		}
	}
}
ds_list_clear(entityToMoveList);
return _switchDir;