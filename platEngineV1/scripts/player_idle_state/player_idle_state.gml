if(is_begin_state()){
	change_sprite(sprite_idle);
}

//set friction if is on ice or not
if(onIce == true) hsp_fric_final = hsp_fric_ice;
else			  hsp_fric_final = hsp_fric_ground;

//and we change a bit if firction is linear or not
hsp_fric_final = lin_fric? hsp_fric_final: hsp_fric_final*(abs(hsp)+1);
		
hsp            = Approach(hsp,0,hsp_fric_final);

var inputX = key_right - key_left;

if(inputX != 0){
	facing = inputX;
	change_state(player_run_state);
}


player_dash();


if(not onground){
	change_state(player_air_state);
} 
entity_motion();

//Actions
player_jump();





 