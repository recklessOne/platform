if(is_begin_state()){
	change_sprite(sprite_jump);
	if(vsp >= 0) {
		change_sprite(sprite_fall);
	}
	
	wallSlide      = false;	
	wallSlideDelay = 0;	
	//set friction and acceleration in the air
	hsp_acc_final  = hsp_acc_air;
	hsp_fric_final = hsp_fric_air;
}

var inputX = key_right - key_left;//1 to right and -1 to left

var _againstWall = (wallRight and key_right) or (wallLeft and key_left);//if against a wall

#region horizontal Speed
if (walljumpdelay == 0){//no air contral during the wall jump, need wait a bit
	 if(inputX == 0){
		//and we change a bit if firction is linear or not
		hsp_fric_final = lin_fric? hsp_fric_final: hsp_fric_final*(abs(hsp)+1);
		hsp            = Approach(hsp,0,hsp_fric_final);	 
	 }
	 else{
		setFacing(inputX);
		//and we change a bit if acceleration is linear or not
		hsp += lin_acc? hsp_acc_final*inputX: (abs(hsp)+1)*hsp_acc_final*inputX; // increase hsp by rate of acceleration in the current direction of movement 
	}
	
	hsp = median(-hsp_walk,hsp,hsp_walk);
}
#endregion
 

#region vertical speed

//gravity depending of the jump type
if(vsp > 0) {
	var grv_final = grv; // if not on a wall we fall at this speed
	change_sprite(sprite_fall);
}
else{ grv_final = grv_rise;}


// standard maximum falling speed
var vsp_max_final = vsp_max; 


#region wall Sliding
if (_againstWall and vsp > 0){
	//small time to begin wall slide
	if(not wallSlide){
		wallSlide      = true;	
		wallSlideDelay = wallSlideDelay_max;
	}	
	change_sprite(sprite_wall);
	grv_final     = grv_wall; // were on a wall so change our gravity to wall gravity. in this case we slow down.
	vsp_max_final = vsp_max_wall; // max falling speed while against wall
}
else if(not _againstWall) {
	if(sprite_index == sprite_wall) change_sprite(sprite_fall); // revert to jump/fall sprite
	wallSlide = false;	
}

if(wallSlideDelay > 0){
	grv_final = 0;
	vsp       = 0;
}
#endregion

vsp += grv_final; // fall at our required speed.

vsp = min(abs(vsp),abs(vsp_max_final))*sign(vsp); // Removed max speed condition since was limiting spring speed

#endregion
platformon = instance_place_wall(x,y+1);
if platformon != noone and object_is_ancestor(platformon.object_index, oEntity) { vsp = platformon.vsp;}
entity_motion();


if(onground and vsp >= 0){
	change_state(player_idle_state); // change to idle state
	/* 
	recover all abilities. if you wish for the first jump or dash
	not to count then check in step event if we are on ground. it
	will reset the value back to max before the player leaves ground. 
	
	i like it this way because it feels more consistent. 
	*/
	jumps = jumpMax;
	dashes	       = dash_max;
	is_jump_spring = false;
}

//Actions
player_jump();
player_ledge_grab();
player_dash();


