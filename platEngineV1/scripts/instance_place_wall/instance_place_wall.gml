///@param x
///@param y
//check a wall, push block or an entity at pos (x,y)
var xx = argument0;
var yy = argument1;

var _wallList = ds_list_create();
var _wallNum = instance_place_list(xx,yy, par_collidable,_wallList,false);
var _wall = noone;
for(var _w = _wallNum -1; _w >= 0; _w--){
	var _wallCheck = ds_list_find_value(_wallList,_w);
	if _wallCheck != noone and instance_exists(_wallCheck) and _wallCheck != id{
		_wall = _wallCheck;
		break;
	}
}

ds_list_destroy(_wallList)
return _wall;
