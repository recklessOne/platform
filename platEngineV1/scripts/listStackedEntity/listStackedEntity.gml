///@param list
///@param baseEntity
///@param dx
///@param dy

//return a list of objects taht stacked over the platform

var _list   =  argument[0]; // list to receive the updated values
var _entity =  argument[1]; //the platform where step on
var dx      =  argument[2]; //dx shift checking
var dy      =  argument[3]; //dy shift checking

// an array that that contain entry of 2 values, one that contain the object_index, and next if it is solid or not.
var _array = [oPlayer,false,oPushBlock,true];




///each entry has 2 values , the instance id and if it is solid or not
for(var i = 0;i < array_length_1d(_array);i += 2){
	var _objType = _array[i];
	var _isSolid = _array[i+1];
	
	with(_objType){if(place_meeting(x+dx,y+dy,_entity))ds_list_add(_list,id,_isSolid);}	
}


//in case if it is over a solid element
for(var i = 0; i < ds_list_size(_list);i+= 2){
	if(_list[|i+1] == false) continue;
	
	var _element = _list[|i];	
	for(var i = 0;i < array_length_1d(_array);i += 2){
		var _objType = _array[i];
		var _isSolid = _array[i+1];
	
		with(_objType){if(place_meeting(x+dx,y+dy,_element) and ds_list_find_index(_list,id) == -1) ds_list_add(_list,id,_isSolid);}	
	}
	
}