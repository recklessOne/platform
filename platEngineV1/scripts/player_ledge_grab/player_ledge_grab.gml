
//need have this skill
if(not ledge_grab_activated) exit;
if(ledgegrabdelay > 0)       exit;

var _dx = wallRight - wallLeft;

if(_dx != 0 and vsp >= 0){
	// determine places to check for a hold to grab - a few pixels in a line
	var ledgecheckx1     = _dx<0 ? bbox_left : bbox_right;
	var ledgecheckx2     = ledgecheckx1 + 3*sign(_dx);
	var ledgechecky      = (bbox_top+grabOffset)-y; // ADJUST THE '+1' to change the height at which the character grabs a ledge
	var ledgecheckystart = ledgechecky;
	
	var ymoved      = 0;
	var _ledgegrab  = collision_line(ledgecheckx1,y+ledgechecky+1,ledgecheckx2,y+ledgechecky+1,oWall,false,true) and
			      not collision_line(ledgecheckx1,y+ledgechecky  ,ledgecheckx2,y+ledgechecky  ,oWall,false,true);
	//testing until ledge grab or pass for all the current vsp. checking x^2 < y^2 has the same result with abs(x) < abs(y)
	while(not _ledgegrab and ymoved*ymoved<vsp*vsp){
		ymoved     += sign(vsp);
		ledgechecky = ledgecheckystart+ymoved;
		_ledgegrab  = collision_line(ledgecheckx1,y+ledgechecky+1,ledgecheckx2,y+ledgechecky+1,oWall,false,true) and 
				  not collision_line(ledgecheckx1,y+ledgechecky  ,ledgecheckx2,y+ledgechecky  ,oWall,false,true);
	}
	
	if(_ledgegrab){
		if _dx < 0 { x += 1;}
		if _dx > 0{ x -= 1;}
		y  += ymoved;
		vsp = 0;
		change_state(player_ledge_grab_state);
	}
}