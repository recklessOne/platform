//special behavious when touch these elements

//Spring
var _spring = oSpring;
var _trigger = false;
if(not place_meeting(x,y,_spring) and place_meeting(x,y+vsp,_spring) and vsp >= 0){
	_trigger = true;
} else if place_meeting(x,y,_spring){
	var _thisSpring = instance_place(x,y,_spring);
	if _thisSpring.triggerOnContact == true{
		_trigger = true;
	}
} else if place_meeting(x,y+vsp,_spring){
	var _thisSpring = instance_place(x,y+vsp,_spring);
	if _thisSpring.triggerOnContact == true{
		_trigger = true;
	}
}
if _trigger{
	for(var v = 0; v < max(vsp,1); v++){
		if place_meeting(x,y+v,par_collidable){
			var _collide = instance_place(x,y+v,par_collidable);
			if instance_exists(_collide) and _collide != id and _collide != noone{
				if !object_is_ancestor(_collide.object_index, oEntity) or _collide.heavy == false{
					return false;
				}
			}
		}
		
		if (!place_meeting(x,y+v-1,_spring) and place_meeting(x,y+v,_spring)){
			var _blockAbove = instance_place(x,y+v-1, oEntity);
			//if _blockAbove == noone or _blockAbove.heavy == 0{
				vsp            = vsp_jump * 1.5; 
				hsp            = hsp/2; // so u dont just fly off towards the left or right but go more upwards	
				push_block(hsp,vsp);
				y = y+v-1;
				is_jump_spring = true;  //so you can released the jump key without reducing the vsp
				return true;
			//}
		} else if (v == 0 and place_meeting(x,y,_spring)){
			var _yAdj = sprite_get_height(sSpring)+1;
			if !place_meeting(x,y-_yAdj,par_collidable){

				vsp            = vsp_jump * 1.5; 
				hsp            = hsp/2; // so u dont just fly off towards the left or right but go more upwards	
				
				push_block(hsp,vsp);
				y -= _yAdj;
				is_jump_spring = true;  //so you can released the jump key without reducing the vsp
				return true;
			} else {
				var _blockAbove = instance_place(x,y-_yAdj, oEntity);
				if _blockAbove != noone and instance_exists(_blockAbove){// and _blockAbove.heavy == false{
					vsp            = vsp_jump * 1.5; 
					hsp            = hsp/2; // so u dont just fly off towards the left or right but go more upwards	
					_blockAbove.y -= _yAdj;
					_blockAbove.vsp = vsp;
					push_block(hsp,vsp);
					y -= _yAdj;
					is_jump_spring = true;  //so you can released the jump key without reducing the vsp
					return true;
				}
			}
		}
	}
}
return false;