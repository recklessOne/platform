///animationHitFrame(frame)
///@param frame

//Returns true if an animation hits a specific


var imageSpeed = sprite_get_speed(sprite_index)/room_speed;

var frame = argument0; // The frame to check for

if(image_speed==0)return false;


if(image_speed>0)return (image_index+imageSpeed >= frame+1) and (image_index < frame+1);
else             return (image_index-imageSpeed <= frame-1) and (image_index > frame-1);
