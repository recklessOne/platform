///@param ignoreID

var _ignoreID = -1; // id of an block that want ignore during the checking

if(argument_count == 1) _ignoreID = argument[0];

//simple case , considering all wallOne blocks
if(_ignoreID == -1){
	return not place_meeting(x,y,oWallOne) and place_meeting(x,y+1,oWallOne) and vsp >= 0;
}
else{
	
	with(oWallOne){
		if(id == _ignoreID) continue;
		if(not place_meeting(x,y,other.id) and place_meeting(x,y-1,other.id) and other.vsp >= 0) return true;
	}
	
	return false;
}


