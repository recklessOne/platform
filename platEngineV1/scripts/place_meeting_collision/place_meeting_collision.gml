///@param x
///@param y
//check a wall, push block or an entity at pos (x,y)
var xx = argument0;
var yy = argument1;

return (not place_meeting(x,y,par_collidable)) and (place_meeting(xx,yy,par_collidable));
//place_meeting(xx,yy, par_collidable);//place_meeting(xx,yy, oWall) || place_meeting(xx,yy, oPushBlock) || place_meeting(xx,yy, oEntity);