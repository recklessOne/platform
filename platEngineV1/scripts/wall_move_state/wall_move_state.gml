var _switchDir = false;
var _x = x;
var _y = y;
var _dir = moveDir;
var _accel = moveAccel;
var _distTraveled = point_distance(_x, _y, startX, startY);
if _distTraveled > moveDistMax * .5{
	//start slowing down when you reach the halfway
	_accel *= -1; 
}
moveSpd =  1;//clamp(moveSpd + _accel,0,moveSpdMax);
var _hSpd = lengthdir_x(moveSpd,_dir);
var _vSpd = lengthdir_y(moveSpd,_dir);
hsp = _hSpd;
vsp = _vSpd;
_x += _hSpd;
_y += _vSpd;
_distTraveled = point_distance(_x, _y, startX, startY);
// if over max dist, then set to max dist then turn around and set my
if _distTraveled > moveDistMax{
//	_x = startX + lengthdir_x(moveDistMax,_dir);
//		_y = startY + lengthdir_y(moveDistMax,_dir);
	_switchDir = true;
}
	
//stop movig if hit wall, with option of that triggering turn
var _collidingObjType = oWall;
if place_meeting(_x,_y,_collidingObjType){
	var _collisionWall = instance_place(_x,_y,_collidingObjType);
	if instance_exists(_collisionWall){
		//set my block to where its right next to the colliding wall
		var _collide = false;
		for(var _w = 0; _w < moveSpd and _collide == false; _w++){
			var _checkX = x + lengthdir_x(_w,moveDir);
			var _checkY = y + lengthdir_y(_w,moveDir);
			if place_meeting(_checkX, _checkY, _collidingObjType){
				_x = x + lengthdir_x(_w-1,moveDir);
				_y = y + lengthdir_y(_w-1,moveDir);
				_collide = true;
				_switchDir = true; // if this is commented out it will jsut stop at the wall
			}
		}
	}
}
_x = round(_x);
_y = round(_y);
var _traveledThisFrame = point_distance(x,y,_x,_y);
var _xMove = lengthdir_x(_traveledThisFrame,_dir);
var _yMove = lengthdir_y(_traveledThisFrame,_dir);
	
x = _x;
y = _y;
var _push = push_block(_xMove,_yMove);
if _push == -2{// or _push == noone{
	_switchDir = true;
}
var _entityCauseSwitch = scr_platformMoveEntity(_xMove,_yMove);
if _switchDir == true or _entityCauseSwitch == true{
	startX = startX + lengthdir_x(moveDistMax,moveDir); 
	startY = startY + lengthdir_y(moveDistMax,moveDir);  
	moveSpd = 0;
	moveDir += 180;
	change_state(wall_pause_state);
}