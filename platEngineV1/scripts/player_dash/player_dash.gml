//perform the dash motion
if(not dash_activated) exit;


if(key_dash and dash_current < 0 and dashes > 0){
	dashes--;
	dash_current = dash_frames;
	dspx         = key_right - key_left;
	dspy         = key_down  - key_up;
	if(dspx == 0 and dspy == 0) dspx = facing;
	var mag		 = (dash_speed/sqrt((dspx*dspx)+(dspy*dspy)));
	dspx		*= mag;
	dspy		*= mag;
	show_debug_message("player Dash ");
	change_state(player_dash_state);
}





