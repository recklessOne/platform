// if jump is pressed and we have jumps available, and were not against and arent doing a wall jump


_againstWall = (onwall == 1 && key_right) or (onwall == -1 && key_left);//if against a wall
onwall = place_meeting(x+3,y,oWall) - place_meeting(x-3,y,oWall);



if(key_jump){
	//wall jump
	if(walljump_activated and _againstWall==1 and !onground){//pressing an the directional input against the wall
		walljumpdelay = walljumpdelay_max; // set the current movement delay to its maximum
		hsp           = -onwall * hsp_wjump; // move us horizontally in the opposite direction to the wall. 
		vsp           = vsp_wjump; // move us vertically
		vsp_frac	  = 0;
		setFacing(-onwall); // face opposite direction of the wall w just jumped from.
		
		begin_state   = true;
	} else if(vsp>vsp_jump and jumps>0){//and _againstWall==0){
		//normal jump or extra jumps. we add delay as a condition to avoid accidental triggers after walljump
		jumps   -= 1; // make sure we can make 1 less jump then before
		vsp      = vsp_jump; // jump based on our normal height.
		vsp_frac = 0;
		change_state(player_air_state);
		
	}
}

// variable jumping
if(variable_jump_activated and not is_jump_spring){
	if(vsp < 0 && (!key_jumpHeld)) vsp = max(vsp,vsp_jumpmin); // if we let go of jump set jump to its min
}