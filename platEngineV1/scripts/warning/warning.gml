///warning(text,value)

//it show this message at Debug Box. Very useful to track values and potential bugs

var str = "";

for(var i = 0;i<argument_count;i++){
	//even entry will treated alike variable names	
	if(i mod 2 == 0) str += string(argument[i]);
	//odd entry will be treated as value
	else		     str += "= "+string(argument[i])+"| ";
}

show_debug_message(str);