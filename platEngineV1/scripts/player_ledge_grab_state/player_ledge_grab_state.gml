if(is_begin_state()){
	change_sprite(sprite_climb); // change to correct sprite
	// incase of an exact press when walking off ledge make sure we grab ledge facing right direction.
	if(wallLeft==1) setFacing(-1); else setFacing(1);  
	image_speed = 0; // first frame of climb animation is the grab state
	phase       = 0;
}

#region Phase 0 : hanging in the edge

if(phase == 0){
	var _mod = 3;
	if platformon != noone
	and !position_meeting(bbox_right+_mod,y,platformon)
	and !position_meeting(bbox_left-_mod,y,platformon)
	and !position_meeting(x,bbox_bottom+_mod,platformon)
	and !position_meeting(x,bbox_top-_mod,platformon)
	{
		platformon = noone;
		change_state(player_air_state);
	} else {
		if platformon != noone{
			var _dirToPlat = point_direction(x,y,platformon.x,platformon.y);
			if !place_meeting(x,y,platformon){
				while !place_meeting(x+lengthdir_x(2,_dirToPlat),y,platformon){
					x += lengthdir_x(1,_dirToPlat);
				}
			}
		}
	}
	if(key_up){
		phase++;
		image_speed = 1;		
	}	
	
	if(key_jump){
		//drop from the ledge
		if(key_down){
			walljumpdelay = 1; // just so regular jump code isnt initiated
			ledgegrab     = false;
			vsp           = 0;
		}
		//wall jump
		else if((wallLeft && key_right) || (wallRight && key_left)){
			if(walljump_activated){
				walljumpdelay = walljumpdelay_max;
				hsp           = -facing * hsp_wjump; // move us horizontally in the opposite direction to the wall. 
				vsp           = vsp_wjump; // move us vertically
				vsp_frac	  = 0;
			} 
		}
		//a vertical ledge jump
		else {
			walljumpdelay = 7; // just so u cant move against the wall initially just looks wierd
			vsp           = vsp_jump;
			vsp_frac	  = 0;
		}
		//in both cases go to air state
		vsp_frac = 0;
		ledgegrabdelay = ledgegrabdelay_max;
		change_state(player_air_state);
	}
}
#endregion 


//Phase 1 : climb the edge animation
else if(phase == 1){
	if(animation_end()){
		
		image_index = image_number - 1;
		// adjust our final position to be ontop of ledge
		x += facing*climb_disp_x;
		y -= climb_disp_y;
		while(place_meeting_wall(x,y))y--;
		change_sprite(sprite_idle); // how we will look once we are on ledge
		change_state(player_idle_state); 
	}
}
