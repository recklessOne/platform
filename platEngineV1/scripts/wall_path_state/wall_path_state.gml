/// @description Insert description here

//check if path is done
if(not path_checked){
	path_checked = true;
	
	if(my_path != noone){
		moving = true;
	}
}


if(moving) and (my_path != noone){
	prev_x               = x;
	prev_y               = y;
	
	my_path_position    += my_path_speed;
	
	if(path_get_closed(my_path)){
		my_path_position = frac(1+my_path_position);
		x				 = round(path_get_x(my_path,my_path_position));
		y				 = round(path_get_y(my_path,my_path_position));
	}else{
		if(my_path_position >= 2)  my_path_position -= 2;
		if(my_path_position < 0  ) my_path_position += 2;
		var mpp          = min(my_path_position,2-my_path_position);
		x                = round(path_get_x(my_path,mpp));
		y                = round(path_get_y(my_path,mpp));
	}
	
	//shifting of the moving platforms at direction x and y
	var dirx        = (x-prev_x);
	var diry        = (y-prev_y);
	scr_platformMoveEntity(dirx,diry);
	
}
