{
    "id": "1cd43c72-31c9-44e0-85e1-0a85ccd77c5b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWallBump",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b8d4b6b3-3acf-442f-bd54-553b343ea225",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "419a229e-806d-4b90-8612-0b26532dde12",
    "visible": true
}