{
    "id": "27cc2b55-0cf8-4976-b25b-57a0d947a38c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMovingComplex",
    "eventList": [
        {
            "id": "0bd4fd48-64f6-47a9-a0ad-b6405e68fb8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27cc2b55-0cf8-4976-b25b-57a0d947a38c"
        },
        {
            "id": "64819b13-9bb4-43f9-b6d7-eb27a4cf11e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "27cc2b55-0cf8-4976-b25b-57a0d947a38c"
        },
        {
            "id": "7ae0880e-8820-4144-8676-bae0602d2fa3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "27cc2b55-0cf8-4976-b25b-57a0d947a38c"
        },
        {
            "id": "8621d7d8-34af-430a-8850-7bbf9ba63222",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "27cc2b55-0cf8-4976-b25b-57a0d947a38c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3d51259e-f813-4c85-9031-ec79bd58c099",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9caf73b9-3ae5-473c-b38e-3f500d6a1b6f",
    "visible": true
}