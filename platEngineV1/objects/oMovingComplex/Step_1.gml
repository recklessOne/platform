/// @description Insert description here

/*
//check if path is done
if(not path_checked){
	path_checked = true;
	
	if(my_path != noone){
		moving = true;
	}
}


if(moving){
	prev_x               = x;
	prev_y               = y;
	
	my_path_position    += my_path_speed;
	
	if(path_get_closed(my_path)){
		my_path_position = frac(1+my_path_position);
		x				 = round(path_get_x(my_path,my_path_position));
		y				 = round(path_get_y(my_path,my_path_position));
	}else{
		if(my_path_position >= 2)  my_path_position -= 2;
		if(my_path_position < 0  ) my_path_position += 2;
		var mpp          = min(my_path_position,2-my_path_position);
		x                = round(path_get_x(my_path,mpp));
		y                = round(path_get_y(my_path,mpp));
	}
	
	var _platformID = id;
	//shifting of the moving platforms at direction x and y
	var dirx        = sign(x-prev_x);
	var diry        = sign(y-prev_y);
	
	// forming the statement this way helps it keep working even if the game ever gets updated to have more than one player
	with(oEntity){
		
		// if the player is on this platform - either standing on it or ledge-grabbing on to it
		if(platformon == _platformID){
			x = _platformID.x + platformrelx;
			y = _platformID.y + platformrely;
			
			if(place_meeting(x,y,oWall)){
				x = _platformID.prev_x + platformrelx - dirx*2;
				y = _platformID.prev_y + platformrely - diry*2;
				
				if(dirx != 0)
					while(!place_meeting_wall(x+dirx,y     )) x+= dirx;
				if(diry != 0)
					while(!place_meeting_wall(x     ,y+diry)) y+= diry;
			}
		}
		//otherwhise it just overlaping it
		else if(place_meeting(x,y,_platformID)){
			// if we can move a player up/down to push it away, then do so
			if(not place_meeting(x,y+diry,oWall))while(place_meeting(x,y,oWall))y += diry;
			else{
				
				if(place_meeting(x,y+diry,oWallOne) or place_meeting(x,y+diry,oFallPlatform) or place_meeting(x,y+diry,oFallPlatform2) )
					y += diry;
				else
					instance_destroy();
			}
		}
	}
}
