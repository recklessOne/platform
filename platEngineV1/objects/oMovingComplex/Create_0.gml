/// @description Insert description here

// set this in instance creation code to make a platform move
my_path          = noone;

// set these to change behaviour of platform along path
my_path_speed    = 0.001;//0.0025; // path goes from 0 to 1, so this goes along whole path in 400 frames
my_path_position = 0; // begin at start of path - i.e. first point defined in path editor

// these will then get set automatically in first frame
path_checked     = false;
moving           = false;
prev_x           = x;
prev_y           = y;

//move variables
moveDistMax = 150;
moveDir = 90;
moveSpd = 0;
moveSpdMax = 1;
moveAccel = .01;
moving = true;
startX = x;
startY = y;
entityToMoveList = ds_list_create();
pushable = false;
hsp = 0;
vsp = 0;
pauseTime = 30;
pauseTimer = 0;
heavy = 3;
change_state(wall_pause_state);
//change_state(wall_move_state);
//change_state(wall_path_state);
//pushList = ds_list_create();