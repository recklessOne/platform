{
    "id": "879107a5-11b9-4000-be41-0f1f23dbdf2c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLever",
    "eventList": [
        {
            "id": "5c84df37-55c7-4b46-96ad-7710bb944f64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "879107a5-11b9-4000-be41-0f1f23dbdf2c"
        },
        {
            "id": "7af0a4e6-7093-4cfb-b052-1f6f3e32bdca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "879107a5-11b9-4000-be41-0f1f23dbdf2c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "061199e9-72ff-4c57-9011-ce9d8424f058",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "19f6dca3-a674-401f-a191-37b0b16856d6",
    "visible": true
}