{
    "id": "8706b028-0b98-4d04-8219-f2af0df0af49",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPushBlock",
    "eventList": [
        {
            "id": "467cf512-8495-453b-b5de-7a9613c107ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8706b028-0b98-4d04-8219-f2af0df0af49"
        },
        {
            "id": "95ff1283-e928-4385-9195-480792d9d7be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "8706b028-0b98-4d04-8219-f2af0df0af49"
        },
        {
            "id": "9d90ab24-b5f7-4539-8278-d262993b6260",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8706b028-0b98-4d04-8219-f2af0df0af49"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9e3a00f1-1d4c-4b47-8a38-c9bb3c4b4ea5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0a5ca719-9d9e-4f4c-a1b9-108e98e6a74e",
    "visible": true
}