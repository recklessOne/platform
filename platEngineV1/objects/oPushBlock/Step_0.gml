event_inherited();

if(onground){
//	vsp      = 0;
	//vsp_frac = 0;
}
else{
	vsp += grv; 
	// fall at our required speed.		
}

vsp = min(vsp,vsp_max); // Removed max speed condition since was limiting spring speed


entity_motion();

//Calc current status
//if(onground){

platformon = instance_place_wall(x,y+1);
if platformon != noone and object_is_ancestor(platformon.object_index, oEntity) { 
	if !heavy{
		vsp = platformon.vsp;
	}else{
		vsp = platformon.vsp * .5;
		platformon.vsp *= .5;
	}
}
	//if (platformon == noone) platformon = instance_place(x,y+1,oPushBlock); 
//}

if(platformon != noone){
	platformrelx = x - platformon.x;
	platformrely = y - platformon.y;	
}