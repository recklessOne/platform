hsp_push_block         = 2;    // max speed when pushing a block
				       
// Deccelleration      
hsp_fric_ground        = 0.9;  // rate of deccelleration if on ground (higher takes longer)
hsp_fric_ice           = 0.04;
hsp_fric_air           = 0.7; // rate of deccelleration if in air (higher takes longer)
		
// Gravity			   
grv					   = 0.3; // rate at which we fall normally
grv_final			   = grv;
vsp_max                = 10; // maximum falling speed

facing         = 1; // 1 right , -1 left
hsp            = 0; // current horizontal movement speed 
vsp            = 0; // current vertical movement speed
vsp_jump	   = 64;
vsp_jump       = -sqrt(2*grv_final*vsp_jump);
hsp_frac       = 0;
vsp_frac       = 0;
onIce          = false;//if over ice or not
onground	   = false; // are we on ground?
platformon     = noone; // what platform are we stood on?  (If moving, helps us move appropriately)
platformrelx   = 0; // relative position to platform we are stood on (if any)
platformrely   = 0;
onwall		   = false; // are we touching a wall?
wallLeft       = false;
wallRight      = false;

pushable = true;
heavy = true;
//pushList = ds_list_create();