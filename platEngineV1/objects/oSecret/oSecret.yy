{
    "id": "cee4a073-0e52-49a4-93b4-4a27b2d51998",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSecret",
    "eventList": [
        {
            "id": "2e0858e1-f47c-4a4b-8df2-7f3c1cd99068",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cee4a073-0e52-49a4-93b4-4a27b2d51998"
        },
        {
            "id": "a5466e13-02ec-4f5c-964e-23595420fdcd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e0310b68-a8de-47d7-90ee-f17ddcb4bae7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cee4a073-0e52-49a4-93b4-4a27b2d51998"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9caf73b9-3ae5-473c-b38e-3f500d6a1b6f",
    "visible": true
}