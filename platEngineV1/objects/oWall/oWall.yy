{
    "id": "3d51259e-f813-4c85-9031-ec79bd58c099",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWall",
    "eventList": [
        {
            "id": "4a83c169-2448-483c-8110-ff63a84825c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d51259e-f813-4c85-9031-ec79bd58c099"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1386ba81-e6c7-4408-b6a7-2a9eda49094a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9caf73b9-3ae5-473c-b38e-3f500d6a1b6f",
    "visible": true
}