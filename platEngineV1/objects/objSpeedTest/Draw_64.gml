num++

//JUMP CURVE

if instance_exists(oPlayer)
{
	ds_list_add(speed_list,oPlayer.y)
}

if ds_list_size(speed_list)>100
{
	ds_list_delete(speed_list,0)
}

draw_set_colour(c_white)
for(i=0;i<ds_list_size(speed_list);i++)
{
	if (i+num) mod 5 ==0
	{
		draw_set_colour(c_red)
	}
	else
	{
		draw_set_colour(c_white)
	}
	draw_circle(50+i*8,200+speed_list[|i],3,false)
}
draw_set_colour(c_white)


//HORIZONTAL SPEED CURVE

/*

if instance_exists(oPlayer)
{
	ds_list_add(speed_list,abs(oPlayer.hsp))
}

if ds_list_size(speed_list)>100
{
	ds_list_delete(speed_list,0)
}

draw_set_colour(c_white)
for(i=0;i<ds_list_size(speed_list);i++)
{
	if (i+num) mod 5 ==0
	{
		draw_set_colour(c_red)
	}
	else
	{
		draw_set_colour(c_white)
	}
	draw_circle(50+i*8,200-speed_list[|i]*30,3,false)
}
draw_set_colour(c_white)
