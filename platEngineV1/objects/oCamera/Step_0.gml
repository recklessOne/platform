/// @description camera modes

// cams current position
cx = camera_get_view_x(view_camera[0]);
cy = camera_get_view_y(view_camera[0]);

switch(mode){
	case cam.follow_object:
		if(!instance_exists(following)) break;
		cx = following.x - (view_w/2) - 15; // i gave it an offset of 15 cause i felt like it wasnt quite centre.
		cy = following.y - (view_h/2) - 15;
	break;
	
	case cam.follow_mouse_drag:
		var mx = display_mouse_get_x(); 
		var my = display_mouse_get_y();
		
		if(mouse_check_button(mb_left)){
			// multiplier will change drag speed.  
			cx += (mouse_xprevious-mx) * 0.5
			cy += (mouse_yprevious-my) * 0.5
		}
		
		// update the variable with what the last coordinates were
		mouse_xprevious = mx;
		mouse_yprevious = my;
		
	break;
	
	case cam.follow_mouse_border:
		// if mouse is within 10 percent of screen (movable area), if not then...
		if(!point_in_rectangle(mouse_x, mouse_y, cx+(view_w*0.1), cy+(view_h*0.1), cx+(view_w*0.9), cx+(view_h*0.9))){
			cx = lerp(cx, mouse_x-(view_w/2), 0.05); // 0.05 is the speed camera will move at
			cy = lerp(cy, mouse_y-(view_h/2), 0.05);
		};
	break;
	
	case cam.follow_mouse_peek:
		// 0.45 determines how far away the character can be from mouse in view area
		cx = lerp(following.x,mouse_x, 0.45)-(view_w/2); 
		cy = lerp(following.y,mouse_y, 0.45)-(view_h/2);
	break;
	
	case cam.move_to_target:
		cx = lerp(cx, target_x - (view_w/2), 0.1);
		cy = lerp(cy, target_y - (view_h/2), 0.1);
	break;
	
	case cam.move_to_follow_object:
		if(!instance_exists(following)) break; // if exists
		// move towards it
		cx = lerp(cx, following.x - (view_w/2), 0.1);
		cy = lerp(cy, following.y - (view_h/2), 0.1);
		// if distance is really small then follow it.
		if(point_distance(cx, cy, following.x - (view_w/2), following.y - (view_h/2)) <1){
				mode = cam.follow_object;
		}
	break;
}

if(bound){
	cx = clamp(cx, 0, room_width-view_w);
	cy = clamp(cy, 0, room_height-view_h);
}

// sets all our new changes made in above modes
camera_set_view_pos(view_camera[0], cx, cy); 