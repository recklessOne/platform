/// @description camera setup

/*
credit to FriendlyCosmonaut. 
this cam is modified code from one of her tutorial videos.
i found this to be the most powerful and easiest systems around.
*/

// Camera Setup, this is basically like the room editor

// amount of level that can be seen on screen. 
view_w = 512;
view_h = 384;

window_width = 1024;
window_height = 768;

window_set_size(window_width,window_height); // resize window of application
// resize application to avoid black space issues.
surface_resize(application_surface,window_width,window_height); 
window_center();

// camera settings
view_camera[0] = camera_create_view(0, 0, view_w, view_h, 0, -1, 0, 0, 180, 180);
	

// camera modes
enum cam {
	follow_object,			// immediately follows another object
	follow_mouse_drag,		// if you click and drag mouse move around screen
	follow_mouse_border,	// if mouse is within 10 percent of screen edge, move
	follow_mouse_peek,		// platform shooter cam, follows player in respect to mouse position
	move_to_target,			// move to specific location in level
	move_to_follow_object,  // move to another object, and then follow it.
}

mode = cam.follow_object; // what mode the camera starts in
following = oPlayer; // object we will follow when apllicable
bound = true; // are we confined to the room itself?

// x and y coordinates for the move_to_target mode 
target_x = 200;
target_y = 200;

mouse_xprevious = -1;
mouse_yprevious = -1;
