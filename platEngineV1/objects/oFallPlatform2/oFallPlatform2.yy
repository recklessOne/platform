{
    "id": "009f5300-1209-47e0-9c03-fb7fcf93b0ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFallPlatform2",
    "eventList": [
        {
            "id": "6d9e31db-9e40-4a61-a8aa-081647240609",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "009f5300-1209-47e0-9c03-fb7fcf93b0ba"
        },
        {
            "id": "b5c12524-e1cd-4ce7-846c-f5f0f836f5d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "009f5300-1209-47e0-9c03-fb7fcf93b0ba"
        },
        {
            "id": "69669be3-b8eb-4207-9890-fa77d0db5156",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "009f5300-1209-47e0-9c03-fb7fcf93b0ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b52c95b-cfb8-4b0a-b3c0-2220557ff6a6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c0290a13-17b0-463e-b2da-7b77ce75c885",
    "visible": true
}