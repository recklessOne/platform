//check heros and enemies over a moving platform
var _list = ds_list_create();

listStackedEntity(_list,id,0,1);

//stand still until someone step on
if(phase == 0){
	var check = false;
	
	/*with(oPlayer){
		if(not onground) continue;
		if(place_meeting(x,y+1,other.id) and not place_meeting(x,y,other.id)){
			check = true;
			break;		
		}
	}*/
	
	if(place_meeting(x,y-1,oPlayer) and oPlayer.onground and y > oPlayer.y){
		phase++; 
		timer = time;
	}
}
//small pause
else if(phase == 1){
	if(--timer <= 0)phase++;	
}
//falls
else if(phase == 2){
	vsp += grv;
	
	var _endCamY = camera_get_view_y(view_camera[0]) + camera_get_view_height(view_camera[0]);
	
	if(bbox_top >= _endCamY){
		phase++;
		timer    = respawnTime;
		vsp      = 0;
		vsp_frac = 0;
		y        = 0;
		visible  = false;
	}
}
//respawn to the orginal position
else if(phase == 3){
	if(--timer <= 0){
		phase   = 0;
		y       = ystart;
		visible = true;
	}	
}


#region now we move the instances with the falling platform
vsp_frac  += vsp;
var vyNew  = round(vsp_frac);
vsp_frac  -= vyNew;

var  dy    = sign(vsp);

repeat(abs(vyNew)) {
	if(vsp > 0)warning("vsp",vsp,"size",ds_list_size(_list));
	y += dy; /// the falling platform is free of interaction with others solids
}
#endregion


ds_list_destroy(_list);