/// @description Insert description here
// You can write your code in this editor
gamepad_set_axis_deadzone(0,0.3);
// Inherit the parent event
event_inherited();

hsp_push_block = 1;


//set the player sprites
sprite_climb = sPlayerClimb;
sprite_wall  = sPlayerWall;
sprite_jump  = sPlayerJump;
sprite_fall  = sPlayerFall;
sprite_run   = sPlayerRun;
sprite_idle  = sPlayerStand;
heavy = 0;
