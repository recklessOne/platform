{
    "id": "e0310b68-a8de-47d7-90ee-f17ddcb4bae7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "c63f5a21-d5ca-4f47-bdb9-c29d92b5a210",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e0310b68-a8de-47d7-90ee-f17ddcb4bae7"
        },
        {
            "id": "04fb644a-fffd-4fc1-983a-61e4f60a0dd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e0310b68-a8de-47d7-90ee-f17ddcb4bae7"
        },
        {
            "id": "304b7f30-8c8f-4768-b39d-669ea1c54b94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0310b68-a8de-47d7-90ee-f17ddcb4bae7"
        }
    ],
    "maskSpriteId": "ba928b81-3409-4859-b1a6-9d1092b7187a",
    "overriddenProperties": null,
    "parentObjectId": "9e3a00f1-1d4c-4b47-8a38-c9bb3c4b4ea5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d8d3ccdb-ac5d-4a37-b7c1-88b15422a1f9",
    "visible": true
}