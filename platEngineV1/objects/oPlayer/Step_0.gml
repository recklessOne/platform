 /// @desc Get Inputs 
set_player_input();

event_inherited();

script_execute(state);

//timers
if(walljumpdelay > 0)  walljumpdelay--;
if(ledgegrabdelay > 0) ledgegrabdelay--;
if(wallSlideDelay > 0) wallSlideDelay--;

if keyboard_check_released(ord("R")){
	room_restart();
}
if key_interact && place_meeting(x,y,oLever)
{
	var inst=instance_place(x,y,oLever)
	with(inst)
	{
		active=!active
	}
}
//crush
var _crush = false;
var _collideObj = par_collidable;
var _mod = 3;
if dead == false{ 
	if (place_meeting(x-_mod,y,_collideObj) and place_meeting(x+_mod,y,_collideObj)){
		var _wallLeft = instance_place(x-_mod,y,_collideObj);
		var _wallRight = instance_place(x+_mod,y,_collideObj);
		if _wallLeft != _wallRight and _wallLeft.bbox_right + 1 < _wallRight.bbox_left -1
		and (_wallLeft.pushable == false)// or _wallLeft.heavy == true) 
		and (_wallRight.pushable == false){// or _wallRight.heavy == true) { // dont get crushed by pushblocks
			_crush = true;
		}
	}
	
	if (place_meeting(x,y-_mod,_collideObj) and place_meeting(x,y+_mod,_collideObj)){
		var _wallTop = instance_place(x,y-_mod,_collideObj);
		var _wallBottom = instance_place(x,y+_mod,_collideObj);
		if _wallTop != _wallBottom and _wallTop.bbox_bottom + 1 < _wallBottom.bbox_top -1
		and (_wallTop.pushable == false or _wallTop.heavy == true) 
		and (_wallBottom.pushable == false or _wallBottom.heavy == true){
			_crush = true;
		}
	}
	
	if (/*place_meeting_wallone() or*/ place_meeting(x,y+1,oSpring)) and place_meeting(x,y-_mod,_collideObj){
		var _wallTop = instance_place(x,y-_mod,_collideObj);
		if _wallTop.pushable == false or _wallTop.heavy == true{
			_crush = true;
		}
	}
	if _crush{
		change_state(player_dead_state);
	}
}