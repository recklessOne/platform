{
    "id": "81131cef-be3e-4ca4-af24-5b2d017a84eb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpring",
    "eventList": [
        {
            "id": "29df4f14-a4c4-4894-935b-655b41ef39eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "81131cef-be3e-4ca4-af24-5b2d017a84eb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "02ecd068-ce12-497a-8938-0e88c057d416",
    "visible": true
}