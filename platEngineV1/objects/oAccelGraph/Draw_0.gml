var xx      = x;
var yy      = y;
var _yScale = 5;
var _xScale = 2;

//draw graph's axis

draw_set_color(c_black);
draw_arrow(xx,yy,xx  ,yy-100,4);
draw_arrow(xx,yy,xx+time*_xScale,yy  ,4);

drawGraph(linearList  ,c_red ,_xScale,_yScale,xx,yy);
drawGraph(nolinearList,c_blue,_xScale,_yScale,xx,yy);