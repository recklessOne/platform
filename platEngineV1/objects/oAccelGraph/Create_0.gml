// Accelleration & Horizontal Limits
lin_acc                = true;
lin_fric               = true;
hsp_acc                = 0.25//0.3; // rate of horizontal acceleration (new super mario bros uses 0.3 there abouts)
hsp_acc_ice            = 0.08; //rate of horizontal acceleration om ice surface
hsp_push_block         = 2;    // max speed when pushing a block
hsp_walk               = 3;    // max movement speed
				       
// Deccelleration      
hsp_fric_ground        = 0.9;  // rate of deccelleration if on ground (higher takes longer)
hsp_fric_ice           = 0.04;
hsp_fric_air           = 0.15; // rate of deccelleration if in air (higher takes longer)

hsp_walk               = 6; 
hsp                    = 0;

linearList   = ds_list_create();
nolinearList = ds_list_create();

time         = 50;
//Graph for linear acc
var timer    = time;

do {
	ds_list_add(linearList,hsp);
	hsp += hsp_acc;
	hsp  = min(hsp,hsp_walk);	
}until(--timer <= 0)

//Graph for no linear acc
hsp       = 0;
var timer = time;

do {
	ds_list_add(nolinearList,hsp);
	hsp += (1+abs(hsp))*hsp_acc;
	hsp  = min(hsp,hsp_walk);	
}until(--timer <= 0)

