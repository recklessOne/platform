{
    "id": "6f1b4325-dade-4ee8-b57f-97dfca46fccc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWallSlope",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b8d4b6b3-3acf-442f-bd54-553b343ea225",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bf8781c1-0a01-4978-9ebe-3385d2a80070",
    "visible": true
}