{
    "id": "7233f34d-e6de-4601-9c71-5e8d8d0fd4e5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWallSlope1",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b8d4b6b3-3acf-442f-bd54-553b343ea225",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "540dd61f-38aa-42d9-9cb7-a22a5cac31be",
    "visible": true
}