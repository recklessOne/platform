{
    "id": "69175537-692c-433c-bed1-3ac084d4d95c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWallBump1",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b8d4b6b3-3acf-442f-bd54-553b343ea225",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "69e0c568-1df6-4ca0-ad2a-770c7f572252",
    "visible": true
}