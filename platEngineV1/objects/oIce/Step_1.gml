
if(!path_checked){
	path_checked=true;
	
	if(my_path!=noone){
		moving=true;
	}
}


if(moving){
	prev_x=x;
	prev_y=y;
	
	if(path_get_closed(my_path)){
		my_path_position=frac(1+my_path_position+my_path_speed);
		x=round(path_get_x(my_path,my_path_position));
		y=round(path_get_y(my_path,my_path_position));
	}else{
		my_path_position=my_path_position+my_path_speed;
		if(my_path_position>=2)my_path_position-=2;
		if(my_path_position<0)my_path_position+=2;
		var mpp = min(my_path_position,2-my_path_position);
		x=round(path_get_x(my_path,mpp));
		y=round(path_get_y(my_path,mpp));
	}
	
	with(oPlayer){
		// forming the statement this way helps it keep working even if the game ever gets updated to have more than one player
		var player=id;
		with(other){	
			if(player.platformon==id){ // if the player is on this platform - either standing on it or ledge-grabbing on to it
				
				player.x=player.platformrelx+x;
				player.y=player.platformrely+y;
				
				with(player)if(place_meeting(x,y,oWall)){
					
					// PLAYER HAS BEEN SQUASHED WHILE ON PLATFORM
					
					// reverse the platform for now
					with(other){
						x = prevx;
						y = prevy;
						my_path_speed*=-1;
						player.x=player.platformrelx+x;
						player.y=player.platformrely+y;
					}
					
				}
				
			}else{ // if the player isn't already on this platform 
				if(place_meeting(x,y,player)){ // the moving platform's new position is intersecting the player
			
					var dx = x-prev_x;
					var dy = y-prev_y;
					
					with(player){
						// if we can move a player up/down to push it away, then do so
						show_debug_message("ICE wile loop start");
						if(!place_meeting(x,y+dy,oWall))while(place_meeting(x,y,oWall))y+=sign(dy);
						// otherwise try pushing left/right
						else if(!place_meeting(x+dx,y,oWall))while(place_meeting(x,y,oWall))x+=sign(dx);
						// if that doesn't work, we have a problem
						else{
							
							// PLAYER HAS BEEN SQUASHED BETWEEN TWO PLATFORMS
							
							// How should this be handled?  Player death?  (Not in code yet)
							// For now, reverse the moving platform
							
							other.x -= dx;
							other.y -= dy;
							other.my_path_speed*=-1;
							
						}
					}
					
				
				}
			}
		}
	}
	
}
