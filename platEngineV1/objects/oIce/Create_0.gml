/// @description Insert description here
// You can write your code in this editor

// set this in instance creation code to make a platform move
my_path = noone;

// set these to change behaviour of platform along path
my_path_speed=0.0025; // path goes from 0 to 1, so this goes along whole path in 400 frames
my_path_position=0; // begin at start of path - i.e. first point defined in path editor

// these will then get set automatically in first frame
path_checked=false;
moving=false;
prev_x=x;
prev_y=y;
