{
    "id": "cfd3dff3-3e21-4b3e-84ce-229bd63fb7dd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oIce",
    "eventList": [
        {
            "id": "73e782d2-b7e9-4aab-bd90-c62aaf03fc9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cfd3dff3-3e21-4b3e-84ce-229bd63fb7dd"
        },
        {
            "id": "6c40e95d-8428-47ac-a463-48066cb051f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "cfd3dff3-3e21-4b3e-84ce-229bd63fb7dd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3d51259e-f813-4c85-9031-ec79bd58c099",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8a330d27-c420-4582-8b49-7e2583393a3f",
    "visible": true
}