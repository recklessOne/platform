
//check if over the ground
//in onground , dash and jump are recovered
onground = false;
var _leftWall = noone;
var _rightWall = noone;
wallLeft = false;

if place_meeting_collision(x-2,y){// or place_meeting(x-1,y,par_collidable){
	var _wallList = instance_place_wall_list(bbox_left-1,y);
	for(var _wl = 0; _wl < ds_list_size(_wallList); _wl ++){
		var _wallCheck = ds_list_find_value(_wallList,_wl);
		if instance_exists(_wallCheck) and _wallCheck != noone
		and _wallCheck.bbox_right < bbox_left{
			wallLeft = true;
			_leftWall = _wallCheck;
		}
	}
}

wallRight = false;
if place_meeting_collision(x+2,y){
	var _wallList = instance_place_wall_list(bbox_right+1,y);
	for(var _wl = 0; _wl < ds_list_size(_wallList); _wl ++){
		var _wallCheck = ds_list_find_value(_wallList,_wl);
		if instance_exists(_wallCheck) and _wallCheck != noone
		and _wallCheck.bbox_left > bbox_right{
			wallRight = true;
			_rightWall = _wallCheck;
		}
	}
	ds_list_destroy(_wallList);
}
var _add = 2;
if place_meeting_collision(x,y+_add){
	 var _wallList = instance_place_wall_list(x,bbox_bottom +_add);
	 for(var _wl = 0; _wl < ds_list_size(_wallList); _wl ++){
		 var _wallCheck = ds_list_find_value(_wallList,_wl);
		if (instance_exists(_wallCheck) and _wallCheck != noone and _wallCheck.bbox_top > bbox_bottom)
		and _wallCheck != _leftWall and _wallCheck != _rightWall{
		var _col = false;
			for(var a = _add; a > 0 and _col == false; a--){
				if !place_meeting_collision(x,y+a) and place_meeting_collision(x,y+a+1) and !place_meeting_wallone(){
					scr_blockOnMe(a);
					y = floor(y+a);
					_col = true;
				}
			}
			onground = true;
	}	}
	ds_list_destroy(_wallList);
}
if place_meeting_wallone(){
	var _wallCheck = instance_place(x,y+1,oWallOne);
	if (instance_exists(_wallCheck) and _wallCheck != noone and _wallCheck.bbox_top > bbox_bottom){
		onground = true;
	}
}

#region turn on/off linear acc
if keyboard_check_released(ord("1")){
	lin_acc = not lin_acc;
	show_message("Linear Acceleration: "+string(lin_acc))
}

if keyboard_check_released(ord("2")){
	lin_fric = not lin_fric;
	show_message("Linear Deceleration: "+string(lin_fric))
}
#endregion




//Calc current status
if(onground){
	platformon = instance_place_wall(x,y+1); // is there a platform under us?	
}

if(platformon != noone){
	platformrelx = x - platformon.x;
	platformrely = y - platformon.y;	
}

