{
    "id": "9e3a00f1-1d4c-4b47-8a38-c9bb3c4b4ea5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEntity",
    "eventList": [
        {
            "id": "1b5b4140-df1f-4a63-b13a-dd5c387c433c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e3a00f1-1d4c-4b47-8a38-c9bb3c4b4ea5"
        },
        {
            "id": "e84774d0-983a-4424-8dfe-f0d085e076e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9e3a00f1-1d4c-4b47-8a38-c9bb3c4b4ea5"
        },
        {
            "id": "ced660da-86b4-4cde-b94f-eec38cc3473d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9e3a00f1-1d4c-4b47-8a38-c9bb3c4b4ea5"
        },
        {
            "id": "99194ab4-4738-47a4-ac55-28067f1a7df7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "9e3a00f1-1d4c-4b47-8a38-c9bb3c4b4ea5"
        },
        {
            "id": "61acde90-72b3-4477-95a2-75f1f3b06201",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "9e3a00f1-1d4c-4b47-8a38-c9bb3c4b4ea5"
        },
        {
            "id": "97782e81-65d5-4a81-b789-d88fdd30e174",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "9e3a00f1-1d4c-4b47-8a38-c9bb3c4b4ea5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1386ba81-e6c7-4408-b6a7-2a9eda49094a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}