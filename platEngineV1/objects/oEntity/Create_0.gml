#region Mechanics 

jumpMax                 = 2; // max number of jumps possible
walljump_activated      = true;
variable_jump_activated = true;
ledge_grab_activated    = true; // whether can grab ledges
dash_activated		    = false; // whether dashing is currently enabled
#endregion

//Customizable options for ingame stuff 

// Accelleration & Horizontal Limits
lin_acc                = true;
lin_fric               = true;
hsp_acc                = 0.9//0.3; // rate of horizontal acceleration (new super mario bros uses 0.3 there abouts)
hsp_acc_ice            = 0.08; //rate of horizontal acceleration om ice surface
hsp_acc_air            = 0.9;
hsp_push_block         = 2;    // max speed when pushing a block
hsp_walk               = 3;    // max movement speed
				       
// Deccelleration      
hsp_fric_ground        = 0.9;  // rate of deccelleration if on ground (higher takes longer)
hsp_fric_ice           = 0.04;
hsp_fric_air           = 0.7; // rate of deccelleration if in air (higher takes longer)
		
//for climbing onto ledge
climbing               = false;
climb_disp_x		   = 12;
climb_disp_y		   = 19;
		
// Gravity			   
grv					   = 0.3; // rate at which we fall
grv_rise			   = 0.3; // gravity applied during jump
grv_wall			   = 0.1; // rate we fall while against a wall
grv_final			   = grv;
		
// Jumping		       
vsp_jump               = 64; // jump height in pixels
vsp_jumpmin            = 12; // minimum jump height in pixels
vsp_max                = 10; // maximum falling speed
vsp_max_wall           = 4; // falling speed when against a wall
vsp_threshold          = vsp_jump/2;//speed threshold

// Wall Jumping	       
hsp_wjump              = 36; // speed we jump away from wall horizontally in pixels
vsp_wjump              = 44; // speed we jump away from wall vertically in pixels
// amount of frames we cant move after wall jump
walljumpdelay_max	   = 17; // max amount. 7 allows for a single wall, 17 requires two walls.
wallSlideDelay_max     = 0;		



#region Dashing
dash_frames            = 10; // time that will be spent dashing
dash_speed             = 10; // speed of the dash
dash_max               = 2; // how many times can dash without touching ground
dash_stop_speed        = 4; // speed to decelerate to when dash ends. // higher amount allows u retain momentum from the dash
dash_stop_when_release = false; // allow variable length dashing
#endregion

#region ledge grab
ledgegrab          = false; // are we grabbing ledge?
grabOffset		   = -2; // offset the y position the character is drawn grabbing the wall
ledgegrabdelay     = 0; // currentamount (set so that, when jump from ledge, won't immediately grab hold again)
ledgegrabdelay_max = 10; // mainly just to make sure we drop down a bit before grabbing again
#endregion


// sliding down slopes that are to steep, remove comments from line 13. this is still WIP
maxslopegradient       = 1.05; // max gradient of a slope that we can stand on (if you only know the angle, set the gradient to dtan(angleFromHorizontal)
slopeslidefactor       = 0.4; // amount that gradient (if more than maxslopegradient) will be turned to hsp


#region Engine Variables, Dont change 
vsp_jump       = -sqrt(2*grv_final*vsp_jump); // do math for jump down here so its easier for people
vsp_jumpmin    = -sqrt(2*grv_final*vsp_jumpmin); // do math for minimum jump down here so its easier for people
hsp_wjump      = sqrt(2*grv_final*hsp_wjump); // speed that we jump away from wall horizontally
vsp_wjump      = -sqrt(2*grv_final*vsp_wjump);
facing         = 1; // 1 right , -1 left
wallSlideDelay = 0;
hsp            = 0; // current horizontal movement speed 
vsp            = 0; // current vertical movement speed
hsp_frac       = 0;
vsp_frac       = 0;
onIce          = false;//if over ice or not
wallSlide      = false; // allow us to jump if it is wall sliding
hsp_acc_final  = hsp_acc;
hsp_fric_final = hsp_fric_ground;
is_jump_spring = false;// check if jump from spring or not	
jumping		   = false; // if upward movement caused by jump, or by something else like a spring?
onground	   = false; // are we on ground?
platformon     = noone; // what platform are we stood on? x (If moving, helps us move appropriately)
platformrelx   = 0; // relative position to platform we are stood on (if any)
platformrely   = 0;
onwall		   = false; // are we touching a wall?
wallLeft       = false;
wallRight      = false;
pushingblock   = false;
jumps          = 0; // current amount of jumps possible to make
walljumpdelay  = 0; // current amount
dspx           = 0; // | x and y direction and |
dspy           = 0; // | speed of current dash |

dash_current   =-1; // dash ongoing
dashes		   = 0; // how many more times can currently dash before touching ground again

dead		   = false;
pushable	   = true
//pushList = ds_list_create();

current_slope_gradient = 0; // gradient of ground we are stood on (if any)
#endregion

//Initialize keys - set to false by default for objects that will never use them
set_player_input();

change_state(player_idle_state);