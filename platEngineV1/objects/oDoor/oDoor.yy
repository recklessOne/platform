{
    "id": "e2fc1415-3646-4cd1-abf5-db491c9e2773",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDoor",
    "eventList": [
        {
            "id": "75f1e472-84b4-4b99-9cc6-2208e93a6096",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e2fc1415-3646-4cd1-abf5-db491c9e2773"
        },
        {
            "id": "6c0b25cd-f5d7-4188-9eaf-40d05d83f6ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e2fc1415-3646-4cd1-abf5-db491c9e2773"
        },
        {
            "id": "21c38219-8c63-44e2-bb58-8b2981fd98b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e2fc1415-3646-4cd1-abf5-db491c9e2773"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3d51259e-f813-4c85-9031-ec79bd58c099",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "290210d4-f236-4822-ac5f-53bd32a1d603",
    "visible": true
}