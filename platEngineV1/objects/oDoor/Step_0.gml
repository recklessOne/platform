/// @description Insert description here
// You can write your code in this editor
if (ds_list_size(required)==0)
{
	open  = true;
}
else
{
	var _open = true;
	for(i=0;i<ds_list_size(required);i++){
		var _lock_id=required[|i];
		with(oLock){
			if (_lock_id==lock_id){
				_open = active;
			}
		}
		if (_open){
			open = true;
			mask_index = -1; // remove collissions when open
		}
		else {
			open = false;
			mask_index = sDoor;
			break;
		}
	}
}

show_debug_message(mask_index)
image_index = open;
