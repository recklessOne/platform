{
    "id": "728db703-0210-4979-bee7-13fa5bf41ef9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFallPlatform",
    "eventList": [
        {
            "id": "87643cd4-bc60-4100-b85d-97dddd1b936c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "728db703-0210-4979-bee7-13fa5bf41ef9"
        },
        {
            "id": "4f159a02-e87f-40ca-967a-82e4f4fc69e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "728db703-0210-4979-bee7-13fa5bf41ef9"
        },
        {
            "id": "e4f9783b-86a6-44ee-a9d5-f0190e27f883",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "728db703-0210-4979-bee7-13fa5bf41ef9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4b52c95b-cfb8-4b0a-b3c0-2220557ff6a6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c0290a13-17b0-463e-b2da-7b77ce75c885",
    "visible": true
}