{
    "id": "94d162f5-600d-4492-890a-e2c5bbd075f2",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "PlatformPath1",
    "closed": true,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "b619d958-8f99-4185-8292-7899c3d4fd2f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 256,
            "speed": 100
        },
        {
            "id": "c7955a3b-73a6-4126-b698-a256ba467432",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 256,
            "speed": 100
        },
        {
            "id": "ff703d76-9e93-4ed6-b13b-ffb8d44f4088",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 416,
            "speed": 100
        },
        {
            "id": "6d364a10-4924-4a92-b4f1-e996b2238964",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 416,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}