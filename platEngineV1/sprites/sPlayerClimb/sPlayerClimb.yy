{
    "id": "076ce915-4383-4acd-8624-5b4414d464f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerClimb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb80d1e6-b5eb-4301-b398-4425d99c7bb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "076ce915-4383-4acd-8624-5b4414d464f9",
            "compositeImage": {
                "id": "5f271f76-4d33-4e50-9ea9-deaec22f688c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb80d1e6-b5eb-4301-b398-4425d99c7bb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a3ed587-5415-4473-a711-95331b102a73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb80d1e6-b5eb-4301-b398-4425d99c7bb7",
                    "LayerId": "7cd0f9a7-d652-4242-b51a-9a7002db1aa5"
                }
            ]
        },
        {
            "id": "5187e3fa-5224-4645-bcc9-07cab47d5bcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "076ce915-4383-4acd-8624-5b4414d464f9",
            "compositeImage": {
                "id": "957d484e-1d0a-4bcc-9c34-27b60a29e30e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5187e3fa-5224-4645-bcc9-07cab47d5bcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50697846-474d-4287-9bbc-affe2e07a0cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5187e3fa-5224-4645-bcc9-07cab47d5bcf",
                    "LayerId": "7cd0f9a7-d652-4242-b51a-9a7002db1aa5"
                }
            ]
        },
        {
            "id": "31f3e289-b29f-472c-92ae-b0eee5986c7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "076ce915-4383-4acd-8624-5b4414d464f9",
            "compositeImage": {
                "id": "926bebb9-4568-407b-9a05-20defa972077",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31f3e289-b29f-472c-92ae-b0eee5986c7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f08499e9-f8e5-4755-adcd-135b67f330d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31f3e289-b29f-472c-92ae-b0eee5986c7b",
                    "LayerId": "7cd0f9a7-d652-4242-b51a-9a7002db1aa5"
                }
            ]
        },
        {
            "id": "2456009d-47ba-43d8-9c56-f953be4d4d62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "076ce915-4383-4acd-8624-5b4414d464f9",
            "compositeImage": {
                "id": "f88ff6a2-7e83-4489-8ef1-7fbf37af154f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2456009d-47ba-43d8-9c56-f953be4d4d62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "001d07e1-dad8-43ba-b118-28b410d71664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2456009d-47ba-43d8-9c56-f953be4d4d62",
                    "LayerId": "7cd0f9a7-d652-4242-b51a-9a7002db1aa5"
                }
            ]
        },
        {
            "id": "fb60fab6-0324-45ec-8d57-9ccdf1889dab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "076ce915-4383-4acd-8624-5b4414d464f9",
            "compositeImage": {
                "id": "7d6f486c-4434-44ca-8e2c-38a2614d2791",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb60fab6-0324-45ec-8d57-9ccdf1889dab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6955dcc-b309-4e3b-b412-5565dff81924",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb60fab6-0324-45ec-8d57-9ccdf1889dab",
                    "LayerId": "7cd0f9a7-d652-4242-b51a-9a7002db1aa5"
                }
            ]
        },
        {
            "id": "9287cd2c-b548-474b-b0f8-d975011c9e54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "076ce915-4383-4acd-8624-5b4414d464f9",
            "compositeImage": {
                "id": "323d9b65-5023-460e-a095-7980577b151b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9287cd2c-b548-474b-b0f8-d975011c9e54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa7d8755-4973-47d8-aa4a-45c26e7fb043",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9287cd2c-b548-474b-b0f8-d975011c9e54",
                    "LayerId": "7cd0f9a7-d652-4242-b51a-9a7002db1aa5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "7cd0f9a7-d652-4242-b51a-9a7002db1aa5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "076ce915-4383-4acd-8624-5b4414d464f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 9,
    "yorig": 40
}