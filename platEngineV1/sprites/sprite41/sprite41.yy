{
    "id": "540dd61f-38aa-42d9-9cb7-a22a5cac31be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite41",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "feb3a514-97df-4264-a75c-e4a09202f163",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "540dd61f-38aa-42d9-9cb7-a22a5cac31be",
            "compositeImage": {
                "id": "d32fceeb-533a-4535-a887-108d27ef3e22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feb3a514-97df-4264-a75c-e4a09202f163",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1af29bd0-9024-4c04-b6f5-83f97ae1ee68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feb3a514-97df-4264-a75c-e4a09202f163",
                    "LayerId": "91a4152d-8745-4294-a351-5415d83e8ce4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "91a4152d-8745-4294-a351-5415d83e8ce4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "540dd61f-38aa-42d9-9cb7-a22a5cac31be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}