{
    "id": "ccece0ed-4384-4b8b-b7a0-dc888dcac468",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGround1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46ddb3bc-3681-421c-adc6-0a2c6a4df69e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccece0ed-4384-4b8b-b7a0-dc888dcac468",
            "compositeImage": {
                "id": "13b6a921-2401-47a7-835c-b8cb474778f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46ddb3bc-3681-421c-adc6-0a2c6a4df69e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c52e0fb-ddf0-43a3-a2df-e5a17e7ee900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46ddb3bc-3681-421c-adc6-0a2c6a4df69e",
                    "LayerId": "d6f213ac-2e53-46fd-aa33-29c674b04e1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "d6f213ac-2e53-46fd-aa33-29c674b04e1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccece0ed-4384-4b8b-b7a0-dc888dcac468",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}