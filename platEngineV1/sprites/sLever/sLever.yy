{
    "id": "19f6dca3-a674-401f-a191-37b0b16856d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLever",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cea78f7-18df-418c-b447-32eaa5d1e91e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19f6dca3-a674-401f-a191-37b0b16856d6",
            "compositeImage": {
                "id": "22ad2e16-e208-44d3-9d8a-50fac719f719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cea78f7-18df-418c-b447-32eaa5d1e91e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d339ef2-7bcf-4108-8c2a-c592691f17e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cea78f7-18df-418c-b447-32eaa5d1e91e",
                    "LayerId": "54e6bfe0-9a0f-46dd-9e7c-c2b713f522c5"
                }
            ]
        },
        {
            "id": "0c045cb7-365b-4868-9994-9b6fe0297c36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19f6dca3-a674-401f-a191-37b0b16856d6",
            "compositeImage": {
                "id": "12a857e8-b485-4a84-9efc-4c753d8c8697",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c045cb7-365b-4868-9994-9b6fe0297c36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cb45e64-faee-4e8f-996c-c28c0fb8e2a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c045cb7-365b-4868-9994-9b6fe0297c36",
                    "LayerId": "54e6bfe0-9a0f-46dd-9e7c-c2b713f522c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "54e6bfe0-9a0f-46dd-9e7c-c2b713f522c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19f6dca3-a674-401f-a191-37b0b16856d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}