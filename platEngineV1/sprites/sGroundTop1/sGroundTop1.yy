{
    "id": "195fedff-19c6-49b4-9973-2d42cfa0b731",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGroundTop1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5649c337-8781-4583-aa5a-3568d1d14b36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "195fedff-19c6-49b4-9973-2d42cfa0b731",
            "compositeImage": {
                "id": "7208828e-9d4a-451f-968c-00424b9a79e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5649c337-8781-4583-aa5a-3568d1d14b36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c960fad4-f621-4744-87e1-eb698b3c9d16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5649c337-8781-4583-aa5a-3568d1d14b36",
                    "LayerId": "7f760f61-521d-4c2c-acf9-9ae9e30b4963"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "7f760f61-521d-4c2c-acf9-9ae9e30b4963",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "195fedff-19c6-49b4-9973-2d42cfa0b731",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}