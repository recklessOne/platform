{
    "id": "9604c96d-91e7-445e-a03d-432fac1b8fff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpikesSingle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 9,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a4fcd45-4cd9-46e2-8a9f-fb63d40f1c5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9604c96d-91e7-445e-a03d-432fac1b8fff",
            "compositeImage": {
                "id": "6e41b79e-4b58-42a7-a883-252561bbab29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a4fcd45-4cd9-46e2-8a9f-fb63d40f1c5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbaf7583-bf75-401f-bb39-84121faf53c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a4fcd45-4cd9-46e2-8a9f-fb63d40f1c5e",
                    "LayerId": "34811448-ffd7-4da2-9ab1-32a4a0b8ef37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "34811448-ffd7-4da2-9ab1-32a4a0b8ef37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9604c96d-91e7-445e-a03d-432fac1b8fff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 0,
    "yorig": 0
}