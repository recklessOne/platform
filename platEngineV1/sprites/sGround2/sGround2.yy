{
    "id": "3c88ef71-4b4d-49d2-a377-f9507df6c866",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGround2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af522b5c-7d1e-47dd-9038-86230911fe90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c88ef71-4b4d-49d2-a377-f9507df6c866",
            "compositeImage": {
                "id": "19aba511-0e0f-4ec8-afe1-7b06d36d18d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af522b5c-7d1e-47dd-9038-86230911fe90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52e3a1a1-2067-4c08-bc21-5498e9fd3ba2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af522b5c-7d1e-47dd-9038-86230911fe90",
                    "LayerId": "b285ad7b-8522-4d29-bf85-57ce9914935d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "b285ad7b-8522-4d29-bf85-57ce9914935d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c88ef71-4b4d-49d2-a377-f9507df6c866",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}