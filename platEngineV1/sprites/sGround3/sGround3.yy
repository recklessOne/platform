{
    "id": "7bad3ad1-6180-47d2-8a9f-ecc75aea6aae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGround3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7ada959-1f3c-44a6-b19e-ff28c8c84382",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bad3ad1-6180-47d2-8a9f-ecc75aea6aae",
            "compositeImage": {
                "id": "86a2a5ab-8be1-4c54-83d8-0ac4d3fa6cbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7ada959-1f3c-44a6-b19e-ff28c8c84382",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ee954a7-478b-40c6-b351-e40414bfcc73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7ada959-1f3c-44a6-b19e-ff28c8c84382",
                    "LayerId": "b73692be-fec4-456d-a197-62ac7369ddae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "b73692be-fec4-456d-a197-62ac7369ddae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7bad3ad1-6180-47d2-8a9f-ecc75aea6aae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}