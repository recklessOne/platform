{
    "id": "d8d3ccdb-ac5d-4a37-b7c1-88b15422a1f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerStand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 5,
    "bbox_right": 16,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81d4e867-a6df-42c5-af86-c57be37a9e7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8d3ccdb-ac5d-4a37-b7c1-88b15422a1f9",
            "compositeImage": {
                "id": "fc4693cc-c5b2-4f78-a9b0-5ff7a3f0db40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81d4e867-a6df-42c5-af86-c57be37a9e7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e9c741a-ffe6-4fd8-9341-1d79afdb11f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81d4e867-a6df-42c5-af86-c57be37a9e7a",
                    "LayerId": "741fc668-af7c-4f32-8276-f99bd990f24f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "741fc668-af7c-4f32-8276-f99bd990f24f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8d3ccdb-ac5d-4a37-b7c1-88b15422a1f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 19
}