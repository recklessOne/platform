{
    "id": "02e1fd05-e394-445f-9d89-65dd96695932",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGroundTopLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bab913a5-f022-4d75-83d6-18b7039cd0ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02e1fd05-e394-445f-9d89-65dd96695932",
            "compositeImage": {
                "id": "b8545f26-4130-4dc8-808a-a9ca7eb73dfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bab913a5-f022-4d75-83d6-18b7039cd0ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47d14a2a-95bd-418c-89a5-2cd8b37f368b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bab913a5-f022-4d75-83d6-18b7039cd0ea",
                    "LayerId": "a60d0b2a-050b-4934-ab7f-e16a8dffe4d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "a60d0b2a-050b-4934-ab7f-e16a8dffe4d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02e1fd05-e394-445f-9d89-65dd96695932",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}