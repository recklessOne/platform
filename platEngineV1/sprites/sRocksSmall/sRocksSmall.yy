{
    "id": "3e896f8e-91aa-4141-9b77-080b69ae5c44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRocksSmall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 7,
    "bbox_right": 12,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fbe72ec-2716-4e97-b0ea-037dc1cbb6d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e896f8e-91aa-4141-9b77-080b69ae5c44",
            "compositeImage": {
                "id": "ba684d48-9e38-477e-be5b-157e16a39040",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fbe72ec-2716-4e97-b0ea-037dc1cbb6d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbb7c05d-b254-4670-836b-f7995ca0287e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fbe72ec-2716-4e97-b0ea-037dc1cbb6d3",
                    "LayerId": "5a8ec1ae-59d1-48e3-b68b-b45d0dea820e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "5a8ec1ae-59d1-48e3-b68b-b45d0dea820e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e896f8e-91aa-4141-9b77-080b69ae5c44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 0,
    "yorig": 0
}