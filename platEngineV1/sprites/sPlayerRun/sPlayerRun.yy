{
    "id": "af5c5f4c-c1ca-4554-af09-96cafcf42e1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerRun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 5,
    "bbox_right": 25,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0df6242f-70c0-44b2-baaf-7104460f1628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af5c5f4c-c1ca-4554-af09-96cafcf42e1e",
            "compositeImage": {
                "id": "0c9e1d1f-637b-4754-840e-3a7ffece2fed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0df6242f-70c0-44b2-baaf-7104460f1628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce824a53-b758-4683-b6e2-2c3a0cda3bf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0df6242f-70c0-44b2-baaf-7104460f1628",
                    "LayerId": "27c112d8-4430-4e99-a918-4cdacf1db48c"
                }
            ]
        },
        {
            "id": "a8065ae2-dfa6-4920-8a1f-b61b2e494741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af5c5f4c-c1ca-4554-af09-96cafcf42e1e",
            "compositeImage": {
                "id": "b3458d30-e087-411a-b9c2-a4f44a045d4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8065ae2-dfa6-4920-8a1f-b61b2e494741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f8ff4e8-f0a9-4d51-9eae-ec7d8247bcc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8065ae2-dfa6-4920-8a1f-b61b2e494741",
                    "LayerId": "27c112d8-4430-4e99-a918-4cdacf1db48c"
                }
            ]
        },
        {
            "id": "b4eecb1c-5652-4038-b5d4-037b0e36a151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af5c5f4c-c1ca-4554-af09-96cafcf42e1e",
            "compositeImage": {
                "id": "e5653095-a1a2-4a7e-a251-a14679c9ca9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4eecb1c-5652-4038-b5d4-037b0e36a151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af2c9f90-36e8-4b9d-9002-a700a9a1593e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4eecb1c-5652-4038-b5d4-037b0e36a151",
                    "LayerId": "27c112d8-4430-4e99-a918-4cdacf1db48c"
                }
            ]
        },
        {
            "id": "b4f6a27d-8154-457a-b5a7-42cbfbec4ea7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af5c5f4c-c1ca-4554-af09-96cafcf42e1e",
            "compositeImage": {
                "id": "cca30c6d-1351-4519-b961-8e9785697344",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4f6a27d-8154-457a-b5a7-42cbfbec4ea7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df81f86e-1c02-455c-98a1-1c6958b125b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4f6a27d-8154-457a-b5a7-42cbfbec4ea7",
                    "LayerId": "27c112d8-4430-4e99-a918-4cdacf1db48c"
                }
            ]
        },
        {
            "id": "e25a08e3-597f-4e5d-98c3-2cf80d321865",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af5c5f4c-c1ca-4554-af09-96cafcf42e1e",
            "compositeImage": {
                "id": "4edf7324-756c-49fe-b4fa-ffe1b611f257",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e25a08e3-597f-4e5d-98c3-2cf80d321865",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0102c7f-f726-4735-82a2-3fbc5a833d5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e25a08e3-597f-4e5d-98c3-2cf80d321865",
                    "LayerId": "27c112d8-4430-4e99-a918-4cdacf1db48c"
                }
            ]
        },
        {
            "id": "1c658095-a936-41c7-947d-84d839e1e530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af5c5f4c-c1ca-4554-af09-96cafcf42e1e",
            "compositeImage": {
                "id": "009e797c-66d1-44be-9503-3a751cf11556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c658095-a936-41c7-947d-84d839e1e530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b742a397-f01c-4b02-abf1-13dda469b609",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c658095-a936-41c7-947d-84d839e1e530",
                    "LayerId": "27c112d8-4430-4e99-a918-4cdacf1db48c"
                }
            ]
        },
        {
            "id": "abfb67f8-521a-4527-98ad-f9b2bbad1dd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af5c5f4c-c1ca-4554-af09-96cafcf42e1e",
            "compositeImage": {
                "id": "bdd902d7-237c-450f-9861-e4df96585048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abfb67f8-521a-4527-98ad-f9b2bbad1dd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e6d7fd1-1965-4293-8ae2-d781271363dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abfb67f8-521a-4527-98ad-f9b2bbad1dd8",
                    "LayerId": "27c112d8-4430-4e99-a918-4cdacf1db48c"
                }
            ]
        },
        {
            "id": "961842ad-aabf-404d-a8dc-4911f3900c7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af5c5f4c-c1ca-4554-af09-96cafcf42e1e",
            "compositeImage": {
                "id": "e80faa2d-857d-4a93-8646-bd7290bc76b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "961842ad-aabf-404d-a8dc-4911f3900c7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8738f468-db5b-422d-ba3e-fd5c0580a00c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "961842ad-aabf-404d-a8dc-4911f3900c7b",
                    "LayerId": "27c112d8-4430-4e99-a918-4cdacf1db48c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "27c112d8-4430-4e99-a918-4cdacf1db48c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af5c5f4c-c1ca-4554-af09-96cafcf42e1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 19
}