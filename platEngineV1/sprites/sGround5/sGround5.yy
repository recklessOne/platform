{
    "id": "da7e9f77-2e2b-477b-b69d-46622fd90a5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGround5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75ace15e-e0fa-4734-94f5-dd12860cc7d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da7e9f77-2e2b-477b-b69d-46622fd90a5f",
            "compositeImage": {
                "id": "e69841e3-3a19-401f-ab77-997930fc1dc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75ace15e-e0fa-4734-94f5-dd12860cc7d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67ddf391-0bf9-4105-9f64-c408967acd28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75ace15e-e0fa-4734-94f5-dd12860cc7d7",
                    "LayerId": "c432efb5-02c0-42fe-94d7-f36398f3e88e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "c432efb5-02c0-42fe-94d7-f36398f3e88e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da7e9f77-2e2b-477b-b69d-46622fd90a5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}