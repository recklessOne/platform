{
    "id": "d69ae443-cc72-4539-bf3e-9e851ceed5ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGround4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ae8c738-8c61-4d74-8e0d-93c2f9bfb57f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d69ae443-cc72-4539-bf3e-9e851ceed5ec",
            "compositeImage": {
                "id": "3dc89622-4c17-4f10-af5b-7a829531f32c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ae8c738-8c61-4d74-8e0d-93c2f9bfb57f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7d2e671-0fec-4de0-bde7-5eb393df18a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ae8c738-8c61-4d74-8e0d-93c2f9bfb57f",
                    "LayerId": "757a0749-3abe-44b3-ac5b-f1de438c2e36"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "757a0749-3abe-44b3-ac5b-f1de438c2e36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d69ae443-cc72-4539-bf3e-9e851ceed5ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}