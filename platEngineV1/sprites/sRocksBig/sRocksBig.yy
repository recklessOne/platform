{
    "id": "e141fa70-8c9e-4a95-8002-490460d30fe5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRocksBig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "130c91b0-3a10-48b7-91ee-ddfccc3cd5b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e141fa70-8c9e-4a95-8002-490460d30fe5",
            "compositeImage": {
                "id": "5597df82-60f4-468e-bd73-137f1270b0fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "130c91b0-3a10-48b7-91ee-ddfccc3cd5b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46c7a00d-021a-4950-b901-103020f90e69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "130c91b0-3a10-48b7-91ee-ddfccc3cd5b8",
                    "LayerId": "0080f23f-3ad5-4cfa-9cd0-5cbafca3708a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "0080f23f-3ad5-4cfa-9cd0-5cbafca3708a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e141fa70-8c9e-4a95-8002-490460d30fe5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 0,
    "yorig": 0
}