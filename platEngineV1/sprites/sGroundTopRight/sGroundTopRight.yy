{
    "id": "b3d411bf-faa5-4f90-a40b-ee5f49cb4e50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGroundTopRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a6951cf-666c-48af-9947-de56b7bf2ab3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3d411bf-faa5-4f90-a40b-ee5f49cb4e50",
            "compositeImage": {
                "id": "c3ba4546-8b49-4dca-a186-5f318aa9ff4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a6951cf-666c-48af-9947-de56b7bf2ab3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a41c55c7-e566-4388-ac2e-a497b9bd4bfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a6951cf-666c-48af-9947-de56b7bf2ab3",
                    "LayerId": "b2db49fc-02bb-40f4-a022-dd0cf22b1bc4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "b2db49fc-02bb-40f4-a022-dd0cf22b1bc4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3d411bf-faa5-4f90-a40b-ee5f49cb4e50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}