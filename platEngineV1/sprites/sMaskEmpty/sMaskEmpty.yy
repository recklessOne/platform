{
    "id": "44d084bc-8ae1-4b86-a6b0-79dcdcd5f679",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMaskEmpty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b730dc82-3e8f-4f0b-b2ee-a733209e04fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44d084bc-8ae1-4b86-a6b0-79dcdcd5f679",
            "compositeImage": {
                "id": "ed171508-e542-45ba-bdfb-6e1b37b2a645",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b730dc82-3e8f-4f0b-b2ee-a733209e04fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf2935f7-d175-4dde-a808-6960a0a80c72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b730dc82-3e8f-4f0b-b2ee-a733209e04fd",
                    "LayerId": "f487006f-9bc7-4a36-9f74-e16802355aa5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "f487006f-9bc7-4a36-9f74-e16802355aa5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44d084bc-8ae1-4b86-a6b0-79dcdcd5f679",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}