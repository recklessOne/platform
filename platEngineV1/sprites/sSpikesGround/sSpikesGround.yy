{
    "id": "922138c6-0fb4-4f79-a739-5d41000b1897",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpikesGround",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2be02c6-556d-4af0-b015-2caf55e9dee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "922138c6-0fb4-4f79-a739-5d41000b1897",
            "compositeImage": {
                "id": "a32323dd-f8b0-4348-9a53-9f08c02ca18b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2be02c6-556d-4af0-b015-2caf55e9dee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86c25be1-8663-42b0-a508-baacd97c03e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2be02c6-556d-4af0-b015-2caf55e9dee3",
                    "LayerId": "678f9294-13bb-468f-8fa2-9dde8d67fabc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "678f9294-13bb-468f-8fa2-9dde8d67fabc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "922138c6-0fb4-4f79-a739-5d41000b1897",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 0,
    "yorig": 0
}