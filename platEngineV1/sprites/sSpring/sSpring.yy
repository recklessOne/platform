{
    "id": "02ecd068-ce12-497a-8938-0e88c057d416",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpring",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0cbfa4a-60e9-4714-990f-016aee3c96d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02ecd068-ce12-497a-8938-0e88c057d416",
            "compositeImage": {
                "id": "2e1c9594-5c1d-4c02-bc30-4e5cf6bfa736",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0cbfa4a-60e9-4714-990f-016aee3c96d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4711cb6b-d059-458f-abf3-4db80cdadcf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0cbfa4a-60e9-4714-990f-016aee3c96d5",
                    "LayerId": "bc60cbdd-6b1c-4ca4-a699-79cafbfe8365"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "bc60cbdd-6b1c-4ca4-a699-79cafbfe8365",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02ecd068-ce12-497a-8938-0e88c057d416",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}