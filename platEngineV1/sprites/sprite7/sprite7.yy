{
    "id": "69e0c568-1df6-4ca0-ad2a-770c7f572252",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6c7da49-c5ba-4208-813a-938319e48b8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e0c568-1df6-4ca0-ad2a-770c7f572252",
            "compositeImage": {
                "id": "1d6da700-8a67-47dc-8d81-420810879f20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6c7da49-c5ba-4208-813a-938319e48b8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73703e64-a8f1-4454-82f2-8001d227614c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6c7da49-c5ba-4208-813a-938319e48b8f",
                    "LayerId": "bab40cb4-4a4c-4fb6-895a-a3e1b983b1b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bab40cb4-4a4c-4fb6-895a-a3e1b983b1b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69e0c568-1df6-4ca0-ad2a-770c7f572252",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 41,
    "yorig": 7
}