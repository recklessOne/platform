{
    "id": "bac9688f-cd26-4121-9676-0611c3566f4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 2,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d40eb810-c06f-4174-a868-8cc3dcac78fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bac9688f-cd26-4121-9676-0611c3566f4d",
            "compositeImage": {
                "id": "ed98b66c-f859-49d1-8706-2c3a618536d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d40eb810-c06f-4174-a868-8cc3dcac78fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca780546-ca5d-4f21-a87f-88c997e4180b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d40eb810-c06f-4174-a868-8cc3dcac78fb",
                    "LayerId": "1f21072b-b30a-49ad-80fd-de4765d71107"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "1f21072b-b30a-49ad-80fd-de4765d71107",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bac9688f-cd26-4121-9676-0611c3566f4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 2,
    "yorig": 1
}