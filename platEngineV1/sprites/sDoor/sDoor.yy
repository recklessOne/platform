{
    "id": "290210d4-f236-4822-ac5f-53bd32a1d603",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2de9404-4f01-414d-a651-3b34e9e6dd47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "290210d4-f236-4822-ac5f-53bd32a1d603",
            "compositeImage": {
                "id": "752ca8fd-2335-4dc7-915e-4f6b033cab9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2de9404-4f01-414d-a651-3b34e9e6dd47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e7637c9-02cb-4ec3-8136-6918b52acbd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2de9404-4f01-414d-a651-3b34e9e6dd47",
                    "LayerId": "2602259e-4c04-4cdc-a91c-2dc2e33adc04"
                }
            ]
        },
        {
            "id": "487fee90-497e-452d-bba7-71dbacb89ea8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "290210d4-f236-4822-ac5f-53bd32a1d603",
            "compositeImage": {
                "id": "ef7842db-740b-4a24-a84b-f2418204964e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "487fee90-497e-452d-bba7-71dbacb89ea8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f88a4938-256e-44d3-af29-237ebd3b79e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "487fee90-497e-452d-bba7-71dbacb89ea8",
                    "LayerId": "2602259e-4c04-4cdc-a91c-2dc2e33adc04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2602259e-4c04-4cdc-a91c-2dc2e33adc04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "290210d4-f236-4822-ac5f-53bd32a1d603",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}