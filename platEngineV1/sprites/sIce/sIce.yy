{
    "id": "8a330d27-c420-4582-8b49-7e2583393a3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIce",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "882c4718-e9a8-4d6f-8c65-f8c06fb3bd83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a330d27-c420-4582-8b49-7e2583393a3f",
            "compositeImage": {
                "id": "9ac5ceba-1e05-43d1-9b42-31abdcbdaf95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "882c4718-e9a8-4d6f-8c65-f8c06fb3bd83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74fd6971-3149-4dc7-96f0-a1d546d3cf63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "882c4718-e9a8-4d6f-8c65-f8c06fb3bd83",
                    "LayerId": "f41a22aa-6b1d-441e-9811-c4d08b599cdd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f41a22aa-6b1d-441e-9811-c4d08b599cdd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a330d27-c420-4582-8b49-7e2583393a3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}