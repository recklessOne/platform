{
    "id": "0570d13c-664d-4417-ba52-fe1a12622b41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGroundBottomRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fdc2af82-4cf8-4042-9f2d-1d394db65a43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0570d13c-664d-4417-ba52-fe1a12622b41",
            "compositeImage": {
                "id": "3e0da559-f5cd-4b49-b3c7-4c37df506f21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdc2af82-4cf8-4042-9f2d-1d394db65a43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eb47266-d04d-4d37-8f3f-38464227b06d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdc2af82-4cf8-4042-9f2d-1d394db65a43",
                    "LayerId": "44646d5a-2fdf-4ec3-bbb4-e8fa096c1152"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "44646d5a-2fdf-4ec3-bbb4-e8fa096c1152",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0570d13c-664d-4417-ba52-fe1a12622b41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}