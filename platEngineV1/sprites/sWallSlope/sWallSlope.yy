{
    "id": "bf8781c1-0a01-4978-9ebe-3385d2a80070",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWallSlope",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8f19185-ad34-4120-a8cb-81c4127fd540",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf8781c1-0a01-4978-9ebe-3385d2a80070",
            "compositeImage": {
                "id": "5a02f4d1-61d6-40e6-8274-286180af4f7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8f19185-ad34-4120-a8cb-81c4127fd540",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec005922-8957-48cf-a003-f1a3daeefbff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8f19185-ad34-4120-a8cb-81c4127fd540",
                    "LayerId": "752587c3-291e-46be-af3f-1ed18d21f7a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "752587c3-291e-46be-af3f-1ed18d21f7a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf8781c1-0a01-4978-9ebe-3385d2a80070",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}