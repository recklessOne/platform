{
    "id": "f208ade5-dbc8-4752-8b52-8e436323709c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDetail2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "733c967a-1545-4f99-986f-1cfad285a267",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f208ade5-dbc8-4752-8b52-8e436323709c",
            "compositeImage": {
                "id": "6afa5f20-b938-4b23-b421-91dd30e42804",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "733c967a-1545-4f99-986f-1cfad285a267",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c251ba7-4525-4cac-8f50-5bb0c64789e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "733c967a-1545-4f99-986f-1cfad285a267",
                    "LayerId": "a83d59e9-ae23-4d2b-a40d-79ef1d6f4337"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "a83d59e9-ae23-4d2b-a40d-79ef1d6f4337",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f208ade5-dbc8-4752-8b52-8e436323709c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}