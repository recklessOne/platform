{
    "id": "eb8fb8b8-6f84-4a5d-a5b2-6959efb9807b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 3,
    "bbox_right": 25,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 4,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf6cb9c0-29f6-4a2d-8db9-457dadefac98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb8fb8b8-6f84-4a5d-a5b2-6959efb9807b",
            "compositeImage": {
                "id": "ffeee5b1-445f-4421-aa4b-2cd94510585f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf6cb9c0-29f6-4a2d-8db9-457dadefac98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d07e9ba-c37c-4068-82c2-75d2076b6cfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf6cb9c0-29f6-4a2d-8db9-457dadefac98",
                    "LayerId": "0e514513-b2f8-4321-877a-3098a334c8f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "0e514513-b2f8-4321-877a-3098a334c8f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb8fb8b8-6f84-4a5d-a5b2-6959efb9807b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 22
}