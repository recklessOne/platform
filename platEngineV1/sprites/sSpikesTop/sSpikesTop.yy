{
    "id": "236d0269-d431-48d3-8ea0-dd6954107571",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpikesTop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 4,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd41b279-f2e3-4f88-a6b4-0df498322d34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "236d0269-d431-48d3-8ea0-dd6954107571",
            "compositeImage": {
                "id": "69bfeab9-e59a-4a5a-a460-e2674be8c8f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd41b279-f2e3-4f88-a6b4-0df498322d34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1f3d7d4-ebf9-400c-86fd-08b3f1f4e1d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd41b279-f2e3-4f88-a6b4-0df498322d34",
                    "LayerId": "9fabcfb9-f394-4710-916f-a9e1a9727f6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "9fabcfb9-f394-4710-916f-a9e1a9727f6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "236d0269-d431-48d3-8ea0-dd6954107571",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 0,
    "yorig": 0
}