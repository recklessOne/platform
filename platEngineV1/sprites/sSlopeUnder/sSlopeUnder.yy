{
    "id": "a2a370b4-00d2-4172-b5fb-00768c391eeb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlopeUnder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9157fb2d-18c5-4686-a90f-b4ac268a39a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2a370b4-00d2-4172-b5fb-00768c391eeb",
            "compositeImage": {
                "id": "fb3928a9-e47e-46e6-84ac-5203e242a0a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9157fb2d-18c5-4686-a90f-b4ac268a39a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4ba316c-2a5b-47a3-b37d-b23f7aa1cc90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9157fb2d-18c5-4686-a90f-b4ac268a39a2",
                    "LayerId": "18439fb7-ccc1-44ea-a875-2a65d1862cf7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "18439fb7-ccc1-44ea-a875-2a65d1862cf7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2a370b4-00d2-4172-b5fb-00768c391eeb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}