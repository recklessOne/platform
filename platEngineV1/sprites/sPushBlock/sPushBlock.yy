{
    "id": "0a5ca719-9d9e-4f4c-a1b9-108e98e6a74e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPushBlock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0badc139-c146-4606-b7bf-f0705f3b3a49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a5ca719-9d9e-4f4c-a1b9-108e98e6a74e",
            "compositeImage": {
                "id": "717028b1-fa12-4357-aa23-6b4b925c2361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0badc139-c146-4606-b7bf-f0705f3b3a49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7281a03-caf6-438b-a784-5645b1c36398",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0badc139-c146-4606-b7bf-f0705f3b3a49",
                    "LayerId": "e56f801a-7dff-436f-ba32-b5af4f496024"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e56f801a-7dff-436f-ba32-b5af4f496024",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a5ca719-9d9e-4f4c-a1b9-108e98e6a74e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}