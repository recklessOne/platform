{
    "id": "03c13df2-6558-4703-ac36-393b97009d8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "620a74b0-3156-46e6-8c97-3d1a31d5d4b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c13df2-6558-4703-ac36-393b97009d8d",
            "compositeImage": {
                "id": "9b4d4857-41cf-4d17-b1b9-2cc14f3003a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "620a74b0-3156-46e6-8c97-3d1a31d5d4b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3d6b332-87c3-4436-ba77-7f61fb0a408c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "620a74b0-3156-46e6-8c97-3d1a31d5d4b2",
                    "LayerId": "c81738f4-b0c1-4304-a70f-5428c6bc2913"
                }
            ]
        },
        {
            "id": "a1f15eee-a678-473e-bcbd-8b29225ded03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c13df2-6558-4703-ac36-393b97009d8d",
            "compositeImage": {
                "id": "c1cf46f7-5376-427d-bde6-9cf0115d43d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f15eee-a678-473e-bcbd-8b29225ded03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97f5711f-cddb-4519-a09e-e4b9da47aa91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f15eee-a678-473e-bcbd-8b29225ded03",
                    "LayerId": "c81738f4-b0c1-4304-a70f-5428c6bc2913"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c81738f4-b0c1-4304-a70f-5428c6bc2913",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03c13df2-6558-4703-ac36-393b97009d8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}