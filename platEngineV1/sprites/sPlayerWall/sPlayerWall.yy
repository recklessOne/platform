{
    "id": "b075e12f-8888-4073-97ec-623d9b0b97d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 10,
    "bbox_right": 21,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1fd0883-1ed6-46f8-b875-07c6ce3510c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b075e12f-8888-4073-97ec-623d9b0b97d0",
            "compositeImage": {
                "id": "f1b09215-a23e-48d5-bfba-9f94cdbb5549",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1fd0883-1ed6-46f8-b875-07c6ce3510c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4daff841-c64b-4ed6-a400-7ed81d21b19e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1fd0883-1ed6-46f8-b875-07c6ce3510c0",
                    "LayerId": "7f9995ff-4e44-4605-b13b-dbf87b5658c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "7f9995ff-4e44-4605-b13b-dbf87b5658c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b075e12f-8888-4073-97ec-623d9b0b97d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 18,
    "yorig": 13
}