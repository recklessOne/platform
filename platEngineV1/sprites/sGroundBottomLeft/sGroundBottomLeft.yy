{
    "id": "4ba17033-6c6e-4903-8358-2a8c5365d110",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGroundBottomLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae8296bc-3464-4cc2-be35-001c01709a9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ba17033-6c6e-4903-8358-2a8c5365d110",
            "compositeImage": {
                "id": "0001f6e7-9334-46af-b05c-f1131dfc9036",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae8296bc-3464-4cc2-be35-001c01709a9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d3cfbe8-d380-4538-8fca-ec8f7e0bed69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae8296bc-3464-4cc2-be35-001c01709a9d",
                    "LayerId": "ffd0d5de-6f96-4011-a037-10a5963f126f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "ffd0d5de-6f96-4011-a037-10a5963f126f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ba17033-6c6e-4903-8358-2a8c5365d110",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}