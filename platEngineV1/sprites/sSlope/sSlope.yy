{
    "id": "e6e2f45b-38c6-477d-9506-6d0aa70f5d2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlope",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c216686-e20f-468e-8a10-e40bb95e39fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6e2f45b-38c6-477d-9506-6d0aa70f5d2f",
            "compositeImage": {
                "id": "9f075fa6-c6cf-4e0e-a6ab-b801fc52e30d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c216686-e20f-468e-8a10-e40bb95e39fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbb009c9-9738-48c3-ad59-118abcc374c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c216686-e20f-468e-8a10-e40bb95e39fd",
                    "LayerId": "bac09bd4-2a6d-4ec8-a377-a1a3cf2c51a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "bac09bd4-2a6d-4ec8-a377-a1a3cf2c51a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6e2f45b-38c6-477d-9506-6d0aa70f5d2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}