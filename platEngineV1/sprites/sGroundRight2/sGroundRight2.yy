{
    "id": "5b523713-05e3-48c9-914a-aee7a35f0d89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGroundRight2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f200cb2d-1216-4b85-aaa5-29041194f07a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b523713-05e3-48c9-914a-aee7a35f0d89",
            "compositeImage": {
                "id": "1b458bb8-fcf5-418d-a729-fea86c7ee184",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f200cb2d-1216-4b85-aaa5-29041194f07a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff6c9e17-edf3-406b-bb71-d464cf7ce013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f200cb2d-1216-4b85-aaa5-29041194f07a",
                    "LayerId": "3d127a26-87c2-40aa-9252-89954ce9e8fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "3d127a26-87c2-40aa-9252-89954ce9e8fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b523713-05e3-48c9-914a-aee7a35f0d89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}