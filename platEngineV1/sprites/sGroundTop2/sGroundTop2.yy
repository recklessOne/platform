{
    "id": "7a227b01-e041-49ce-9c31-09e20a230485",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGroundTop2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb88d0e8-1e89-4027-910d-817247023e3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a227b01-e041-49ce-9c31-09e20a230485",
            "compositeImage": {
                "id": "e7167de4-af70-4406-80f5-da8d7764ac4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb88d0e8-1e89-4027-910d-817247023e3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "044d04d6-04ae-401e-8c8b-45503db4afc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb88d0e8-1e89-4027-910d-817247023e3d",
                    "LayerId": "e731178d-8058-4f82-94a5-796dffb4802a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "e731178d-8058-4f82-94a5-796dffb4802a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a227b01-e041-49ce-9c31-09e20a230485",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 8
}