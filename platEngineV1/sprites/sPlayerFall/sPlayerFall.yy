{
    "id": "7a56558b-dd89-4ed9-a742-d1722fba8ae7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerFall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 2,
    "bbox_right": 24,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e6566db-399e-45b6-a627-0c8c24bad1c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a56558b-dd89-4ed9-a742-d1722fba8ae7",
            "compositeImage": {
                "id": "4a3a68ca-7f2e-4939-a759-8d695fdb7781",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e6566db-399e-45b6-a627-0c8c24bad1c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0792617d-8cba-46ac-b81d-2704a7545e66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e6566db-399e-45b6-a627-0c8c24bad1c5",
                    "LayerId": "cca4a27d-ade9-4708-bbd1-add08b417bd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cca4a27d-ade9-4708-bbd1-add08b417bd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a56558b-dd89-4ed9-a742-d1722fba8ae7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 19
}