{
    "id": "419a229e-806d-4b90-8612-0b26532dde12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe7ccd7e-d061-48bb-a1eb-979ae4d78903",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419a229e-806d-4b90-8612-0b26532dde12",
            "compositeImage": {
                "id": "d1f8ff2c-1018-4a78-ae93-9fd0d1fc5f93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe7ccd7e-d061-48bb-a1eb-979ae4d78903",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd252323-3588-4074-8af2-ae45e3f4170b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe7ccd7e-d061-48bb-a1eb-979ae4d78903",
                    "LayerId": "c4541496-e77d-4c6a-a87c-561a1199ec81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c4541496-e77d-4c6a-a87c-561a1199ec81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "419a229e-806d-4b90-8612-0b26532dde12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 8
}