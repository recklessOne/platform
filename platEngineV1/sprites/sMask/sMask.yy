{
    "id": "ba928b81-3409-4859-b1a6-9d1092b7187a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 2,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf4573ae-17f4-4a37-aa95-588be8dca54c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba928b81-3409-4859-b1a6-9d1092b7187a",
            "compositeImage": {
                "id": "205672d3-f9a0-4eb4-9929-1c1e0fcec565",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf4573ae-17f4-4a37-aa95-588be8dca54c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06b9452b-a358-4f93-8d86-90d01d57e42c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf4573ae-17f4-4a37-aa95-588be8dca54c",
                    "LayerId": "84405bd6-af1a-4205-8fce-74a1f4af6253"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "84405bd6-af1a-4205-8fce-74a1f4af6253",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba928b81-3409-4859-b1a6-9d1092b7187a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 10
}